--liquibase formatted sql

--changeset Alexey Danilov:6204f513-d532-4364-ae06-af3fed6a03df
create table if not exists docker_compose
(
    id           bigserial not null
        constraint docker_compose_pkey
            primary key,
    created_date timestamp,
    model_name   varchar(255),
    created      boolean,
    open_port    bigint,
    url          varchar(255),
    owner        varchar(255),
    password     varchar(255),
    user_name    varchar(255),
    database     varchar(255)
);

--changeset Alexey Danilov:18ed881d-c157-4ed2-aaf0-391b6071bcf7
create table if not exists version
(
    id                bigserial not null
        constraint version_pkey
            primary key,
    created_date      timestamp,
    deployed          boolean default false,
    version_name      varchar(255),
    docker_compose_id bigint
        constraint fk8ic8xm7jwgr86f2810cytre4x
            references docker_compose
);

--changeset Alexey Danilov:101eaf35-fd18-47c5-8cfb-bfb70e25202e
create table if not exists add_column
(
    id                bigserial not null
        constraint add_column_pkey
            primary key,
    is_auto_increment boolean,
    created_date      timestamp,
    name              varchar(255),
    is_nullable       boolean,
    is_primary_key    boolean,
    table_name        varchar(255),
    type              varchar(255),
    is_unique         boolean,
    version_id        bigint
        constraint fk98l1rgx30hm581qluc6nccscl
            references version
);

--changeset Alexey Danilov:56159edf-9bd9-45d3-a0d6-140b1f6360b6
create table if not exists create_table
(
    id           bigserial not null
        constraint table_pkey
            primary key,
    created_date timestamp,
    name         varchar(255),
    version_id   bigint
        constraint fkais06q6y2a52u5wn4pe30i41o
            references version
);

--changeset Alexey Danilov:9d5598e8-72f0-48e6-919f-a2c5e7f6ed0f
create table if not exists table_column
(
    id                bigserial not null
        constraint column_pkey
            primary key,
    is_auto_increment boolean,
    created_date      timestamp,
    name              varchar(255),
    is_nullable       boolean,
    is_primary_key    boolean,
    type              varchar(255),
    is_unique         boolean,
    create_table_id   bigint
        constraint fk4e701dlirngbh5shw8jjm4m3w
            references create_table
);

--changeset Alexey Danilov:2435f7a0-1b06-40f8-a328-704df501333b
create table if not exists drop_index
(
    id           bigserial not null
        constraint drop_index_pkey
            primary key,
    name         varchar(255),
    table_name   varchar(255),
    created_date timestamp,
    version_id   bigint references version
);

--changeset Alexey Danilov:5ad0a429-0190-44c2-abfd-f98bc6acfbc6
create table if not exists drop_connection
(
    id              bigserial not null
        constraint drop_connection_pkey
            primary key,
    name            varchar(255),
    base_table_name varchar(255),
    created_date    timestamp,
    version_id      bigint references version
);

--changeset Alexey Danilov:cafdc8d3-bcc9-47ee-aea7-e2ee04465850
create table if not exists create_index
(
    id           bigserial not null
        constraint create_index_pkey
            primary key,
    is_unique    bool,
    created_date timestamp,
    name         varchar(255),
    table_name   varchar(255),
    version_id   bigint references version
);

--changeset Alexey Danilov:f9e652cd-271a-4362-8f91-8bef22441b57
create table if not exists create_index_column
(
    id              bigserial not null
        constraint create_index_column_pkey
            primary key,
    name            varchar(255),
    created_date    timestamp,

    create_index_id bigint references create_index
);

--changeset Alexey Danilov:45b4dcf2-7a06-4042-ae8e-a99522e3ab6a
create table if not exists connection
(
    id                      bigserial not null
        constraint connection_pkey
            primary key,
    base_table_name         varchar(255),
    base_column_names       varchar(255),
    constraint_name         varchar(255),
    created_date            timestamp,
    referenced_table_name   varchar(255),
    referenced_column_names varchar(255),
    version_id              bigint references version
);

--changeset Alexey Danilov:ebd41766-0cf8-4f03-a380-c7bcadb50244
create table if not exists drop_column
(
    id           bigserial not null
        constraint drop_column_pkey primary key,
    table_name   varchar(255),
    created_date timestamp,
    version_id   bigint references version
);

--changeset Alexey Danilov:263cdfbb-cdac-46ba-9302-7cc961d99a60
create table if not exists drop_column_info
(
    id             bigserial not null
        constraint drop_column_info_pkey primary key,
    name           varchar(255),
    created_date   timestamp,
    drop_column_id bigint references drop_column
);

--changeset Alexey Danilov:85ea1964-bc19-481d-8de7-cd1a2c8e7a89
create table if not exists drop_table
(
    id           bigserial not null
        constraint drop_table_info_pkey primary key,
    name         varchar(255),
    created_date timestamp,
    cascade      bool,
    version_id   bigint references version
);

--changeset Alexey Danilov:d4e471a2-a270-4e20-ba34-eb458872e6ad
create table if not exists add_sql
(
    id           bigserial not null
        constraint add_sql_info_pkey primary key,
    created_date timestamp,
    sql          varchar(32768),
    ddl          boolean,
    version_id   bigint references version
);
