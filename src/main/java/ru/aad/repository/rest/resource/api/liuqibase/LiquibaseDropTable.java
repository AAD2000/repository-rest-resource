package ru.aad.repository.rest.resource.api.liuqibase;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@JsonInclude(Include.NON_NULL)
public class LiquibaseDropTable implements Changes {

  private DropTable dropTable;

  public LiquibaseDropTable(LiquibaseCreateTable liquibaseCreateTable) {
    this.dropTable = new DropTable(liquibaseCreateTable.getCreateTable().getTableName(), false);
  }

  public LiquibaseDropTable(ru.aad.repository.rest.resource.api.DropTable dropTable) {
    this.dropTable = new DropTable(dropTable.getName(), dropTable.getCascade());
  }

  @Data
  @AllArgsConstructor
  @JsonInclude(Include.NON_NULL)
  public static class DropTable {

    private String tableName;

    private Boolean cascade;
  }
}
