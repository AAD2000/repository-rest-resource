package ru.aad.repository.rest.resource.repositories;

import org.springframework.data.repository.CrudRepository;
import ru.aad.repository.rest.resource.model.DropIndex;

public interface DropIndexRepository extends CrudRepository<DropIndex, Long> {

}
