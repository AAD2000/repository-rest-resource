package ru.aad.repository.rest.resource.api.liuqibase;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@JsonInclude(Include.NON_NULL)
public class LiquibaseInclude implements LiquibaseChangelog.LiquibaseChangelogChild {

  private Include include;

  public LiquibaseInclude(String file) {
    this.include = new Include(file, true);
  }

  @Data
  @AllArgsConstructor
  @JsonInclude(JsonInclude.Include.NON_NULL)
  public static class Include {

    private String file;

    private Boolean relativeToChangelogFile;
  }
}
