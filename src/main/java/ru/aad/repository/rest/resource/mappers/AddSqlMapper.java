package ru.aad.repository.rest.resource.mappers;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface AddSqlMapper {

  ru.aad.repository.rest.resource.api.AddSql model2Dto(
      ru.aad.repository.rest.resource.model.AddSql model);

  @Mapping(target = "createdDate", expression = "java(dto.getCreatedDate() == null ? java.time.LocalDateTime.now() : dto.getCreatedDate())")
  ru.aad.repository.rest.resource.model.AddSql dto2Model(
      ru.aad.repository.rest.resource.api.AddSql dto);
}
