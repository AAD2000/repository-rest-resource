package ru.aad.repository.rest.resource.model;

import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Data;

@Data
@Entity
@Table(name = "table_column")
public class TableColumn extends IdInterface {

  @Id
  @Column(name = "id")
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Column(name = "is_auto_increment")
  private Boolean autoIncrement;

  @Column(name = "created_date")
  private LocalDateTime createdDate;

  @Column(name = "name")
  private String name;

  @Column(name = "is_nullable")
  private Boolean nullable;

  @Column(name = "is_primary_key")
  private Boolean primaryKey;

  @Column(name = "type")
  private String type;

  @Column(name = "is_unique")
  private Boolean unique;

  @ManyToOne
  @JoinColumn(name = "create_table_id")
  private CreateTable createTable;
}
