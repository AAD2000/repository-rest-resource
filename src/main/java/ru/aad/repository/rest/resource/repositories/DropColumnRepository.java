package ru.aad.repository.rest.resource.repositories;

import org.springframework.data.repository.CrudRepository;
import ru.aad.repository.rest.resource.model.DropColumn;

public interface DropColumnRepository extends CrudRepository<DropColumn, Long> {

}
