package ru.aad.repository.rest.resource.repositories;

import org.springframework.data.repository.CrudRepository;
import ru.aad.repository.rest.resource.model.Connection;

public interface ConnectionRepository extends CrudRepository<Connection, Long> {

}
