package ru.aad.repository.rest.resource.controllers;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.aad.repository.rest.resource.api.TableColumn;
import ru.aad.repository.rest.resource.services.TableColumnService;

@CrossOrigin
@RestController
@AllArgsConstructor
public class TableColumnController {

  private final TableColumnService tableColumnService;

  @PostMapping("/table-column")
  public Long create(
      @RequestParam(name = "v") Long v,
      @RequestParam(name = "ct") Long ct,
      @RequestBody TableColumn entity) {
    return tableColumnService.create(v, ct, entity);
  }

  @DeleteMapping("/table-column/{id}")
  public void delete(@PathVariable(name = "id") Long id) {
    tableColumnService.delete(id);
  }

  @PutMapping("/table-column/{id}")
  public void update(
      @RequestParam(name = "v") Long v,
      @RequestParam(name = "ct") Long ct,
      @PathVariable(name = "id") Long id,
      @RequestBody TableColumn entity) {
    tableColumnService.update(v, ct, id, entity);
  }
}
