package ru.aad.repository.rest.resource.repositories;

import org.springframework.data.repository.CrudRepository;
import ru.aad.repository.rest.resource.model.AddSql;

public interface AddSqlRepository extends CrudRepository<AddSql, Long> {

}
