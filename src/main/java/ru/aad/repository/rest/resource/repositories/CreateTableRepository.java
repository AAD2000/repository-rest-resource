package ru.aad.repository.rest.resource.repositories;

import java.util.Optional;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import ru.aad.repository.rest.resource.model.CreateTable;

public interface CreateTableRepository extends CrudRepository<CreateTable, Long> {

  @Query(
      value =
          "select ct.* from create_table ct inner join version v on ct.version_id = v.id inner join docker_compose dc on v.docker_compose_id = dc.id where dc.model_name = :model and dc.owner = :owner and ct.name = :name ",
      nativeQuery = true)
  Optional<CreateTable> findByNameModelOwner(String model, String owner, String name);
}
