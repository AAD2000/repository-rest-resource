package ru.aad.repository.rest.resource.mappers;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface AddColumnMapper  {

  ru.aad.repository.rest.resource.api.AddColumn model2Dto(
      ru.aad.repository.rest.resource.model.AddColumn model);

  @Mapping(target = "createdDate", expression = "java(dto.getCreatedDate() == null ? java.time.LocalDateTime.now() : dto.getCreatedDate())")
  ru.aad.repository.rest.resource.model.AddColumn dto2Model(
      ru.aad.repository.rest.resource.api.AddColumn dto);
}
