package ru.aad.repository.rest.resource.services;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.aad.repository.rest.resource.api.CreateIndex;
import ru.aad.repository.rest.resource.copiers.Copier;
import ru.aad.repository.rest.resource.mappers.CreateIndexMapper;
import ru.aad.repository.rest.resource.model.Version;
import ru.aad.repository.rest.resource.repositories.CreateIndexRepository;
import ru.aad.repository.rest.resource.repositories.VersionRepository;

@Service
@AllArgsConstructor
public class CreateIndexService {

  private final Copier copier;

  private final CreateIndexMapper mapper;

  private final VersionRepository versionRepository;

  private final CreateIndexRepository entityRepository;

  @Transactional
  public void create(Long v, CreateIndex entity) {
    versionRepository
        .findById(v)
        .orElseThrow(IllegalArgumentException::new)
        .getCreateIndices()
        .add(mapper.dto2Model(entity));
  }

  public void delete(Long id) {
    entityRepository.deleteById(id);
  }

  @Transactional
  public void update(Long v, Long id, CreateIndex entity) {
    final Version version =
        versionRepository.findById(v).orElseThrow(IllegalArgumentException::new);

    if (version.isDeployed()) {
      throw new IllegalArgumentException();
    }

    copier.copy(
        entityRepository.findById(id).orElseThrow(IllegalArgumentException::new),
        mapper.dto2Model(entity));
  }

  public CreateIndex get(Long id) {
    return entityRepository
        .findById(id)
        .map(mapper::model2Dto)
        .orElseThrow(IllegalArgumentException::new);
  }
}
