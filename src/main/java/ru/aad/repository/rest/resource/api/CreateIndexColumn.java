package ru.aad.repository.rest.resource.api;

import java.time.LocalDateTime;
import lombok.Data;

@Data
public class CreateIndexColumn {

  private Long id;

  private String name;

  private LocalDateTime createdDate;
}
