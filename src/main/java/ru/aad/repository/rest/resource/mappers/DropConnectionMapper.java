package ru.aad.repository.rest.resource.mappers;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface DropConnectionMapper {

  ru.aad.repository.rest.resource.api.DropConnection model2Dto(
      ru.aad.repository.rest.resource.model.DropConnection model);

  @Mapping(target = "createdDate", expression = "java(dto.getCreatedDate() == null ? java.time.LocalDateTime.now() : dto.getCreatedDate())")
  ru.aad.repository.rest.resource.model.DropConnection dto2Model(
      ru.aad.repository.rest.resource.api.DropConnection dto);
}
