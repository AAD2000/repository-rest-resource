package ru.aad.repository.rest.resource.helper;

import lombok.experimental.UtilityClass;
import ru.aad.repository.rest.resource.api.DockerCompose;

@UtilityClass
public class FileContent {

  String getImage(DockerCompose dockerCompose) {
    if (dockerCompose.getDatabase().equals("postgres")) {
      return "postgres:11";
    } else {
      return "mysql:5";
    }
  }

  String getLiquibaseImage(DockerCompose dockerCompose) {
    if (dockerCompose.getDatabase().equals("postgres")) {
      return "kilna/liquibase-postgres";
    } else {
      return "kilna/liquibase-mysql";
    }
  }

  String getDriver(DockerCompose dockerCompose) {
    if (dockerCompose.getDatabase().equals("postgres")) {
      return "org.postgresql.Driver";
    } else {
      return "com.mysql.jdbc.Driver";
    }
  }

  String getClassPath(DockerCompose dockerCompose) {
    if (dockerCompose.getDatabase().equals("postgres")) {
      return "/opt/jdbc/postgres-jdbc.jar";
    } else {
      return "/opt/jdbc/mysql-jdbc.jar";
    }
  }

  public String getJdbcPreUrl(String dockerCompose) {
    if (dockerCompose.equals("postgres")) {
      return "jdbc:postgresql://";
    } else {
      return "jdbc:mysql://";
    }
  }

  public String getUserConfigName(DockerCompose dockerCompose) {
    if (dockerCompose.getDatabase().equals("postgres")) {
      return "POSTGRES_USER";
    } else {
      return "MYSQL_USER";
    }
  }

  public String getPasswordConfigName(DockerCompose dockerCompose) {
    if (dockerCompose.getDatabase().equals("postgres")) {
      return "POSTGRES_PASSWORD";
    } else {
      return "MYSQL_PASSWORD";
    }
  }

  public String getDbConfigName(DockerCompose dockerCompose) {
    if (dockerCompose.getDatabase().equals("postgres")) {
      return "POSTGRES_DB";
    } else {
      return "MYSQL_DATABASE";
    }
  }

  public String getPort(DockerCompose dockerCompose) {
    if ((dockerCompose.getDatabase() == null ? "" : dockerCompose.getDatabase())
        .equals("postgres")) {
      return "5432";
    } else {
      return "3306";
    }
  }

  public String dockerComposeRollback(DockerCompose dockerCompose, String tag) {
    if (dockerCompose.getUrl() != null) {
      return String.format(
          "version: \"2.1\"\n"
              + "\n"
              + "services:\n"
              + "  liquibase:\n"
              + "    image: kilna/liquibase-postgres\n"
              + "    volumes:\n"
              + "      - ${PWD}/changelog:/workspace\n"
              + "    command: sh -c \"liquibase rollback %s\"",
          dockerCompose.getOpenPort(),
          dockerCompose.getUserName(),
          dockerCompose.getPassword(),
          dockerCompose.getModelName(),
          tag);
    }

    return String.format(
        "version: \"2.1\"\n"
            + "\n"
            + "services:\n"
            + "  db:\n"
            + "    hostname: db_%s\n"
            + "    image: "
            + getImage(dockerCompose)
            + "\n"
            + "    ports:\n"
            + "      - %s:"
            + getPort(dockerCompose)
            + "\n"
            + "    environment:\n"
            + "      - "
            + getUserConfigName(dockerCompose)
            + "=%s\n"
            + "      - "
            + getPasswordConfigName(dockerCompose)
            + "=%s\n"
            + "      - "
            + getDbConfigName(dockerCompose)
            + "=%s\n"
            + "  liquibase:\n"
            + "    image: "
            + getLiquibaseImage(dockerCompose)
            + "\n"
            + "    volumes:\n"
            + "      - ${PWD}/changelog:/workspace\n"
            + "    command: sh -c \"sleep ${LIQUIBASE_SLEEP:-15} && liquibase rollback %s\"\n"
            + "    depends_on:\n"
            + "      - db\n",
        dockerCompose.getId(),
        dockerCompose.getOpenPort(),
        dockerCompose.getUserName(),
        dockerCompose.getPassword(),
        dockerCompose.getModelName(),
        tag);
  }

  public String dockerComposeUpdate(DockerCompose dockerCompose) {
    if (dockerCompose.getUrl() != null) {
      return String.format(
          "version: \"2.1\"\n"
              + "\n"
              + "services:\n"
              + "  liquibase:\n"
              + "    image: "
              + getLiquibaseImage(dockerCompose)
              + "\n"
              + "    volumes:\n"
              + "      - ${PWD}/changelog:/workspace\n"
              + "    command: sh -c \"liquibase update\"",
          dockerCompose.getOpenPort(),
          dockerCompose.getUserName(),
          dockerCompose.getPassword(),
          dockerCompose.getModelName());
    }

    return String.format(
        "version: \"2.1\"\n"
            + "\n"
            + "services:\n"
            + "  db:\n"
            + "    image: "
            + getImage(dockerCompose)
            + "\n"
            + (dockerCompose.getDatabase().equals("postgres")
                ? ""
                : "    command: --default-authentication-plugin=mysql_native_password\n")
            + "    hostname: db_%s\n"
            + "    ports:\n"
            + "      - %s:"
            + getPort(dockerCompose)
            + "\n"
            + "    environment:\n"
            + "      - "
            + getUserConfigName(dockerCompose)
            + "=%s\n"
            + "      - "
            + getPasswordConfigName(dockerCompose)
            + "=%s\n"
            + (dockerCompose.getDatabase().equals("postgres")
                ? ""
                : ("      - MYSQL_ROOT_PASSWORD=" + dockerCompose.getPassword() + "\n"))
            + "      - "
            + getDbConfigName(dockerCompose)
            + "=%s\n"
            + "  liquibase:\n"
            + "    image: "
            + getLiquibaseImage(dockerCompose)
            + "\n"
            + "    volumes:\n"
            + "      - ${PWD}/changelog:/workspace\n"
            + "    command: sh -c \"sleep 15 && liquibase update\"\n"
            + "    depends_on:\n"
            + "      - db\n",
        dockerCompose.getId(),
        dockerCompose.getOpenPort(),
        dockerCompose.getUserName(),
        dockerCompose.getPassword(),
        dockerCompose.getModelName());
  }

  public String liquibaseProperties(DockerCompose dockerCompose, String ip) {

    String url =
        dockerCompose.getUrl() == null
            ? getJdbcPreUrl(dockerCompose.getDatabase())
                + "db_"
                + dockerCompose.getId()
                + ":"
                + getPort(dockerCompose)
                + "/"
                + dockerCompose.getModelName()
            : dockerCompose.getUrl();

    return String.format(
        "classpath: "
            + getClassPath(dockerCompose)
            + "\n"
            + "driver: "
            + getDriver(dockerCompose)
            + "\n"
            + "url: %s\n"
            + "username: %s\n"
            + "password: %s\n"
            + "changeLogFile: changelog-master.json\n"
            + "logLevel: info\n",
        url,
        dockerCompose.getUserName(),
        dockerCompose.getPassword());
  }
}
