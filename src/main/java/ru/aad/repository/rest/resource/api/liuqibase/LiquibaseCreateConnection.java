package ru.aad.repository.rest.resource.api.liuqibase;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import lombok.AllArgsConstructor;
import lombok.Data;
import ru.aad.repository.rest.resource.api.Connection;

@Data
@JsonInclude(Include.NON_NULL)
public class LiquibaseCreateConnection implements Changes {

  private AddForeignKeyConstraint addForeignKeyConstraint;

  public LiquibaseCreateConnection(Connection connection) {
    this.addForeignKeyConstraint = new AddForeignKeyConstraint(connection.getBaseColumnNames(),
        connection.getBaseTableName(), connection.getConstraintName(),
        connection.getReferencedColumnNames(), connection.getReferencedTableName());
  }

  @Data
  @AllArgsConstructor
  public static class AddForeignKeyConstraint {

    private String baseColumnNames;

    private String baseTableName;

    private String constraintName;

    private String referencedColumnNames;

    private String referencedTableName;
  }
}
