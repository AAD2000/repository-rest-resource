package ru.aad.repository.rest.resource.services;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.aad.repository.rest.resource.api.AddColumn;
import ru.aad.repository.rest.resource.copiers.Copier;
import ru.aad.repository.rest.resource.mappers.AddColumnMapper;
import ru.aad.repository.rest.resource.model.Version;
import ru.aad.repository.rest.resource.repositories.AddColumnRepository;
import ru.aad.repository.rest.resource.repositories.VersionRepository;

@Service
@AllArgsConstructor
public class AddColumnService {

  private final Copier copier;

  private final AddColumnMapper addColumnMapper;

  private final VersionRepository versionRepository;

  private final AddColumnRepository addColumnRepository;

  @Transactional
  public void create(Long v, AddColumn entity) {
    final ru.aad.repository.rest.resource.model.AddColumn addColumn =
        addColumnMapper.dto2Model(entity);

    final Version version =
        versionRepository.findById(v).orElseThrow(IllegalArgumentException::new);

    version.getAddColumns().add(addColumn);
  }

  public void delete(Long id) {
    addColumnRepository.deleteById(id);
  }

  @Transactional
  public void update(Long v, Long id, AddColumn addColumn) {
    final Version version =
        versionRepository.findById(v).orElseThrow(IllegalArgumentException::new);

    if (version.isDeployed()) {
      throw new IllegalArgumentException();
    }

    copier.addColumnCopy(
        addColumnRepository.findById(id).orElseThrow(IllegalArgumentException::new),
        addColumnMapper.dto2Model(addColumn));
  }
}
