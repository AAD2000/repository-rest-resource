package ru.aad.repository.rest.resource.api.liuqibase;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import lombok.AllArgsConstructor;
import lombok.Data;
import ru.aad.repository.rest.resource.api.Version;

@Data
@JsonInclude(Include.NON_NULL)
public class LiquibaseDatabaseTag implements Changes {

  private TagDatabase tagDatabase;

  public LiquibaseDatabaseTag(Version version) {
    this.tagDatabase = new TagDatabase(version.getVersionName().toString());
  }

  public LiquibaseDatabaseTag() {
    this.tagDatabase = new TagDatabase("0.0");
  }

  @Data
  @AllArgsConstructor
  @JsonInclude(Include.NON_NULL)
  public static class TagDatabase {

    private String tag;
  }
}
