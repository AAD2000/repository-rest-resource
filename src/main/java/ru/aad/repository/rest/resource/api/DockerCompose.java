package ru.aad.repository.rest.resource.api;

import java.time.LocalDateTime;
import java.util.List;
import lombok.Data;

@Data
public class DockerCompose {

  private Long id;

  private LocalDateTime createdDate;

  private String modelName;

  private Long openPort;

  private Boolean created;

  private String password;

  private String userName;

  private String owner;

  private String url;

  private String database;

  private List<Version> versions;
}
