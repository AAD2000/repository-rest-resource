package ru.aad.repository.rest.resource.repositories;

import org.springframework.data.repository.CrudRepository;
import ru.aad.repository.rest.resource.model.CreateIndex;

public interface CreateIndexRepository extends CrudRepository<CreateIndex, Long> {

}
