package ru.aad.repository.rest.resource.api.liuqibase;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import java.util.ArrayList;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import ru.aad.repository.rest.resource.api.DropColumnInfo;
import ru.aad.repository.rest.resource.api.liuqibase.LiquibaseAddColumn.AddColumn;

@Data
@JsonInclude(Include.NON_NULL)
public class LiquibaseDropColumn implements Changes {

  private DropColumn dropColumn;

  public LiquibaseDropColumn(ru.aad.repository.rest.resource.api.DropColumn dropColumn) {
    this.dropColumn = new DropColumn(dropColumn);
  }

  public LiquibaseDropColumn(LiquibaseAddColumn liquibaseAddColumn) {

  }

  @Data
  @JsonInclude(Include.NON_NULL)
  public static class DropColumn {

    private String tableName;

    private List<Column> columns;

    public DropColumn(ru.aad.repository.rest.resource.api.DropColumn dropColumn) {
      this.tableName = dropColumn.getTableName();

      columns = new ArrayList<>();

      for (DropColumnInfo d : dropColumn.getDropColumnInfos()) {
        columns.add(new Column(d.getName()));
      }
    }

    public DropColumn(LiquibaseAddColumn liquibaseAddColumn) {
      this.tableName = liquibaseAddColumn.getAddColumn().getTableName();

      this.columns = new ArrayList<>();

      for (AddColumn.Column c : liquibaseAddColumn.getAddColumn().getColumns()) {
        columns.add(new Column(c.getName()));
      }
    }

    @Data
    @AllArgsConstructor
    @JsonInclude(Include.NON_NULL)
    public static class Column {

      private String name;
    }
  }
}
