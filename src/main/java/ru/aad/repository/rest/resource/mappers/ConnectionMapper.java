package ru.aad.repository.rest.resource.mappers;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface ConnectionMapper {

  ru.aad.repository.rest.resource.api.Connection model2Dto(
      ru.aad.repository.rest.resource.model.Connection model);

  @Mapping(target = "createdDate", expression = "java(dto.getCreatedDate() == null ? java.time.LocalDateTime.now() : dto.getCreatedDate())")
  ru.aad.repository.rest.resource.model.Connection dto2Model(
      ru.aad.repository.rest.resource.api.Connection dto);
}
