package ru.aad.repository.rest.resource.model;

import java.time.LocalDateTime;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import lombok.Data;

@Data
@Entity
@Table(name = "create_index")
public class CreateIndex extends IdInterface {

  @Id
  @Column(name = "id")
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Column(name = "is_unique")
  private Boolean unique;

  @Column(name = "name")
  private String name;

  @Column(name = "table_name")
  private String tableName;

  @Column(name = "created_date")
  private LocalDateTime createdDate;

  @OneToMany(orphanRemoval = true, cascade = CascadeType.ALL)
  @JoinColumn(name = "create_index_id")
  private List<CreateIndexColumn> createIndexColumns;
}
