package ru.aad.repository.rest.resource.services;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.aad.repository.rest.resource.api.DropIndex;
import ru.aad.repository.rest.resource.copiers.Copier;
import ru.aad.repository.rest.resource.mappers.DropIndexMapper;
import ru.aad.repository.rest.resource.model.Version;
import ru.aad.repository.rest.resource.repositories.DropIndexRepository;
import ru.aad.repository.rest.resource.repositories.VersionRepository;

@Service
@AllArgsConstructor
public class DropIndexService {

  private final Copier copier;

  private final DropIndexMapper mapper;

  private final VersionRepository versionRepository;

  private final DropIndexRepository entityRepository;

  @Transactional
  public void create(Long v, DropIndex entity) {
    versionRepository
        .findById(v)
        .orElseThrow(IllegalArgumentException::new)
        .getDropIndices()
        .add(mapper.dto2Model(entity));
  }

  public void delete(Long id) {
    entityRepository.deleteById(id);
  }

  @Transactional
  public void update(Long v, Long id, DropIndex entity) {
    final Version version =
        versionRepository.findById(v).orElseThrow(IllegalArgumentException::new);

    if (version.isDeployed()) {
      throw new IllegalArgumentException();
    }

    copier.copy(
        entityRepository.findById(id).orElseThrow(IllegalArgumentException::new),
        mapper.dto2Model(entity));
  }
}
