package ru.aad.repository.rest.resource.api.liuqibase;

import java.util.Collections;
import java.util.List;
import lombok.Data;

@Data
public class LiquibaseInitialTag implements LiquibaseChangelog.LiquibaseChangelogChild {

  private LiquibaseChangeSet changeSet = new LiquibaseChangeSet();

  @Data
  public static class LiquibaseChangeSet {

    private String id = "init";

    private String author = "ODD";

    private List<ZeroDataBaseTag> changes = Collections.singletonList(new ZeroDataBaseTag());

    @Data
    public static class ZeroDataBaseTag {

      private Tag tagDatabase = new Tag();

      @Data
      public static class Tag {

        private String tag = "0.0";
      }
    }
  }
}
