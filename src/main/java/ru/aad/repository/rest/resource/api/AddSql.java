package ru.aad.repository.rest.resource.api;

import java.time.LocalDateTime;
import lombok.Data;

@Data
public class AddSql {

  private Long id;

  private String sql;

  private LocalDateTime createdDate;

  private Boolean ddl;
}
