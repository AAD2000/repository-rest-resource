package ru.aad.repository.rest.resource.api.liuqibase;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import lombok.AllArgsConstructor;
import lombok.Data;
import ru.aad.repository.rest.resource.api.AddSql;

@Data
@JsonInclude(Include.NON_NULL)
public class LiquibaseAddSql implements Changes {

  private Sql sql;

  public LiquibaseAddSql(AddSql addSql) {
    this.sql = new Sql(addSql.getSql());
  }

  @Data
  @AllArgsConstructor
  public static class Sql {

    private String sql;
  }
}
