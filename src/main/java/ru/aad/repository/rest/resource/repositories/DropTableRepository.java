package ru.aad.repository.rest.resource.repositories;

import org.springframework.data.repository.CrudRepository;
import ru.aad.repository.rest.resource.model.DropTable;

public interface DropTableRepository extends CrudRepository<DropTable, Long> {

}
