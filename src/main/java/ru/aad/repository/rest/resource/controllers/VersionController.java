package ru.aad.repository.rest.resource.controllers;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.aad.repository.rest.resource.api.Version;
import ru.aad.repository.rest.resource.services.VersionService;

@CrossOrigin
@RestController
@AllArgsConstructor
public class VersionController {

  private final VersionService versionService;

  @PostMapping("/version")
  public void create(@RequestParam(name = "dc") Long dc, @RequestBody Version version) {
    versionService.create(dc, version);
  }

  @GetMapping("/version/{id}")
  public Version get(@PathVariable(name = "id") Long id) {
    return versionService.get(id);
  }

  @DeleteMapping("/version/{id}")
  public void delete(@PathVariable(name = "id") Long id) {
    versionService.delete(id);
  }
}
