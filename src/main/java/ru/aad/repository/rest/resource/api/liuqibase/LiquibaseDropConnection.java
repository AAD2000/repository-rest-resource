package ru.aad.repository.rest.resource.api.liuqibase;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@JsonInclude(Include.NON_NULL)
public class LiquibaseDropConnection implements Changes {

  private DropForeignKeyConstraint dropForeignKeyConstraint;

  public LiquibaseDropConnection(
      ru.aad.repository.rest.resource.api.DropConnection dropConnection) {
    this.dropForeignKeyConstraint = new DropForeignKeyConstraint(dropConnection.getBaseTableName(),
        dropConnection.getName());
  }

  public LiquibaseDropConnection(LiquibaseCreateConnection liquibaseCreateConnection) {
    this.dropForeignKeyConstraint = new DropForeignKeyConstraint(
        liquibaseCreateConnection.getAddForeignKeyConstraint().getBaseTableName(),
        liquibaseCreateConnection.getAddForeignKeyConstraint().getConstraintName());
  }

  @Data
  @AllArgsConstructor
  @JsonInclude(Include.NON_NULL)
  public static class DropForeignKeyConstraint {

    private String baseTableName;

    private String constraintName;
  }
}
