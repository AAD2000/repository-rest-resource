package ru.aad.repository.rest.resource.mappers;

import java.util.ArrayList;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

@Mapper(componentModel = "spring", uses = VersionMapper.class)
public interface DockerComposeMapper {

  ru.aad.repository.rest.resource.api.DockerCompose model2Dto(
      ru.aad.repository.rest.resource.model.DockerCompose model);

  @AfterMapping
  default void afterModel2Dto(@MappingTarget ru.aad.repository.rest.resource.api.DockerCompose dto) {
    dto.setVersions(dto.getVersions() == null ? new ArrayList<>() : dto.getVersions());
  }

  @Mapping(target = "createdDate", expression = "java(dto.getCreatedDate() == null ? java.time.LocalDateTime.now() : dto.getCreatedDate())")
  ru.aad.repository.rest.resource.model.DockerCompose dto2Model(
      ru.aad.repository.rest.resource.api.DockerCompose dto);
}
