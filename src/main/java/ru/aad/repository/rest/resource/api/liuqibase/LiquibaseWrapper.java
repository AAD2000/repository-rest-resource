package ru.aad.repository.rest.resource.api.liuqibase;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import ru.aad.repository.rest.resource.api.liuqibase.LiquibaseWrapper.LiquibaseDatabaseChangeLog.LiquibaseChangeSet;

@Data
@JsonInclude(Include.NON_NULL)
public class LiquibaseWrapper<R> {

  private List<LiquibaseDatabaseChangeLog<R>> databaseChangeLog;

  public LiquibaseWrapper(String id, Changes changes, LiquibaseDatabaseTag liquibaseDatabaseTag,
      R rollback) {
    this.databaseChangeLog = Collections
        .singletonList(new LiquibaseDatabaseChangeLog<>(new LiquibaseChangeSet<>(
            id, "ODD", Arrays.asList(changes, liquibaseDatabaseTag),
            rollback)));
  }

  @Data
  @AllArgsConstructor
  @JsonInclude(Include.NON_NULL)
  public static class LiquibaseDatabaseChangeLog<R> {

    private LiquibaseChangeSet<R> changeSet;

    @Data
    @AllArgsConstructor
    @JsonInclude(Include.NON_NULL)
    public static class LiquibaseChangeSet<R> {

      private String id;

      private String author;

      private List<Changes> changes;

      private R rollback;
    }
  }
}
