package ru.aad.repository.rest.resource.mappers;

import java.util.ArrayList;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

@Mapper(componentModel = "spring", uses = CreateIndexColumnMapper.class)
public interface CreateIndexMapper {

  ru.aad.repository.rest.resource.api.CreateIndex model2Dto(
      ru.aad.repository.rest.resource.model.CreateIndex model);

  @AfterMapping
  default void afterModel2Dto(@MappingTarget ru.aad.repository.rest.resource.api.CreateIndex dto) {
    dto.setCreateIndexColumns(dto.getCreateIndexColumns() == null ? new ArrayList<>() : dto.getCreateIndexColumns());
  }

  @Mapping(target = "createdDate", expression = "java(dto.getCreatedDate() == null ? java.time.LocalDateTime.now() : dto.getCreatedDate())")
  ru.aad.repository.rest.resource.model.CreateIndex dto2Model(
      ru.aad.repository.rest.resource.api.CreateIndex dto);
}
