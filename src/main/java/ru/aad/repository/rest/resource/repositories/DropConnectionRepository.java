package ru.aad.repository.rest.resource.repositories;

import org.springframework.data.repository.CrudRepository;
import ru.aad.repository.rest.resource.model.DropConnection;

public interface DropConnectionRepository extends CrudRepository<DropConnection, Long> {

}
