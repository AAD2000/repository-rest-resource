package ru.aad.repository.rest.resource.repositories;

import org.springframework.data.repository.CrudRepository;
import ru.aad.repository.rest.resource.model.CreateIndexColumn;

public interface CreateIndexColumnRepository extends CrudRepository<CreateIndexColumn, Long> {

}
