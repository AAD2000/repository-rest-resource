package ru.aad.repository.rest.resource.controllers;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.aad.repository.rest.resource.api.DropColumn;
import ru.aad.repository.rest.resource.services.DropColumnService;

@CrossOrigin
@RestController
@AllArgsConstructor
public class DropColumnController {

  private final DropColumnService dropColumnService;

  @PostMapping("/drop-column")
  public void create(@RequestParam(name = "v") Long v, @RequestBody DropColumn entity) {
    dropColumnService.create(v, entity);
  }

  @DeleteMapping("/drop-column/{id}")
  public void delete(@PathVariable(name = "id") Long id) {
    dropColumnService.delete(id);
  }

  @PutMapping("/drop-column/{id}")
  public void update(
      @RequestParam(name = "v") Long v,
      @PathVariable(name = "id") Long id,
      @RequestBody DropColumn entity) {
    dropColumnService.update(v, id, entity);
  }
}
