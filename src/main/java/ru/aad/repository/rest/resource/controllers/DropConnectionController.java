package ru.aad.repository.rest.resource.controllers;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.aad.repository.rest.resource.api.DropConnection;
import ru.aad.repository.rest.resource.services.DropConnectionService;

@CrossOrigin
@RestController
@AllArgsConstructor
public class DropConnectionController {

  private final DropConnectionService dropConnectionService;

  @PostMapping("/drop-connection")
  public void create(@RequestParam(name = "v") Long v, @RequestBody DropConnection entity) {
    dropConnectionService.create(v, entity);
  }

  @DeleteMapping("/drop-connection/{id}")
  public void delete(@PathVariable(name = "id") Long id) {
    dropConnectionService.delete(id);
  }

  @PutMapping("/drop-connection/{id}")
  public void update(
      @RequestParam(name = "v") Long v,
      @PathVariable(name = "id") Long id,
      @RequestBody DropConnection entity) {
    dropConnectionService.update(v, id, entity);
  }
}
