package ru.aad.repository.rest.resource.controllers;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.aad.repository.rest.resource.api.DropTable;
import ru.aad.repository.rest.resource.services.DropTableService;

@CrossOrigin
@RestController
@AllArgsConstructor
public class DropTableController {

  private final DropTableService dropTableService;

  @PostMapping("/drop-table")
  public void create(@RequestParam(name = "v") Long v, @RequestBody DropTable entity) {
    dropTableService.create(v, entity);
  }

  @DeleteMapping("/drop-table/{id}")
  public void delete(@PathVariable(name = "id") Long id) {
    dropTableService.delete(id);
  }

  @PutMapping("/drop-table/{id}")
  public void update(
      @RequestParam(name = "v") Long v,
      @PathVariable(name = "id") Long id,
      @RequestBody DropTable entity) {
    dropTableService.update(v, id, entity);
  }
}
