package ru.aad.repository.rest.resource.api;

import java.time.LocalDateTime;
import lombok.Data;

@Data
public class DropIndex {

  private Long id;

  private String name;

  private String tableName;

  private LocalDateTime createdDate;
}
