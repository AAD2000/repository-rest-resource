package ru.aad.repository.rest.resource.api;

import java.time.LocalDateTime;
import java.util.List;
import lombok.Data;

@Data
public class DropColumn {

  private Long id;

  private String tableName;

  private LocalDateTime createdDate;

  private List<DropColumnInfo> dropColumnInfos;
}
