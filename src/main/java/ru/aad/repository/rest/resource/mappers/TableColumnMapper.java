package ru.aad.repository.rest.resource.mappers;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface TableColumnMapper {

  ru.aad.repository.rest.resource.api.TableColumn model2Dto(
      ru.aad.repository.rest.resource.model.TableColumn model);

  @Mapping(target = "createdDate", expression = "java(dto.getCreatedDate() == null ? java.time.LocalDateTime.now() : dto.getCreatedDate())")
  ru.aad.repository.rest.resource.model.TableColumn dto2Model(
      ru.aad.repository.rest.resource.api.TableColumn dto);
}
