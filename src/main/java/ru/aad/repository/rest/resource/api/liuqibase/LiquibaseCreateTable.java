package ru.aad.repository.rest.resource.api.liuqibase;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import java.util.ArrayList;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import ru.aad.repository.rest.resource.api.CreateTable;
import ru.aad.repository.rest.resource.api.TableColumn;

@Data
@JsonInclude(Include.NON_NULL)
public class LiquibaseCreateTable implements Changes {

  private LiquibaseCreateTableInner createTable;

  public LiquibaseCreateTable(CreateTable createTable) {
    this.createTable = new LiquibaseCreateTableInner(createTable);
  }

  @Data
  @JsonInclude(Include.NON_NULL)
  public static class LiquibaseCreateTableInner {

    private List<LiquibaseColumnWrapper> columns;

    private String tableName;

    public LiquibaseCreateTableInner(CreateTable createTable) {
      this.tableName = createTable.getName();

      this.columns = new ArrayList<>();

      for (TableColumn t : createTable.getTableColumns()) {
        this.columns.add(new LiquibaseColumnWrapper(t, tableName));
      }
    }

    @Data
    @JsonInclude(Include.NON_NULL)
    public static class LiquibaseColumnWrapper {

      private LiquibaseColumn column;

      public LiquibaseColumnWrapper(TableColumn tableColumn, String tableName) {
        this.column = new LiquibaseColumn(tableColumn, tableName);
      }
    }

    @Data
    @JsonInclude(Include.NON_NULL)
    public static class LiquibaseColumn {


      private String name;

      private String type;

      private Constraints constraints;

      public LiquibaseColumn(TableColumn tableColumn, String tableName) {
        this.name = tableColumn.getName();
        this.type = tableColumn.getType();
        this.constraints = new Constraints(tableColumn.getPrimaryKey(),
            tableColumn.getPrimaryKey() == null ? null : "pk_" + tableName, tableColumn.getUnique(),
            tableColumn.getNullable() == null || tableColumn.getNullable());
      }

      @Data
      @AllArgsConstructor
      @JsonInclude(Include.NON_NULL)
      public static class Constraints {

        private Boolean primaryKey;

        private String primaryKeyName;

        private Boolean unique;

        private Boolean nullable;
      }
    }
  }
}
