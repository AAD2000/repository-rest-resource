package ru.aad.repository.rest.resource.controllers;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.aad.repository.rest.resource.api.AddSql;
import ru.aad.repository.rest.resource.services.AddSqlService;

@CrossOrigin
@RestController
@AllArgsConstructor
public class AddSqlController {

  private final AddSqlService addSqlService;

  @PostMapping("/add-sql")
  public void create(@RequestParam(name = "v") Long v, @RequestBody AddSql entity) {
    addSqlService.create(v, entity);
  }

  @DeleteMapping("/add-sql/{id}")
  public void delete(@PathVariable(name = "id") Long id) {
    addSqlService.delete(id);
  }

  @PutMapping("/add-sql/{id}")
  public void update(
      @RequestParam(name = "v") Long v,
      @PathVariable(name = "id") Long id,
      @RequestBody AddSql entity) {
    addSqlService.update(v, id, entity);
  }
}
