package ru.aad.repository.rest.resource.api.liuqibase;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import java.util.Collections;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@JsonInclude(Include.NON_NULL)
public class LiquibaseAddColumn implements Changes {

  private AddColumn addColumn;

  public LiquibaseAddColumn(ru.aad.repository.rest.resource.api.AddColumn addColumn) {
    this.addColumn = new AddColumn(addColumn);
  }

  @Data
  @JsonInclude(Include.NON_NULL)
  public static class AddColumn {

    private String tableName;

    private List<Column> columns;

    public AddColumn(ru.aad.repository.rest.resource.api.AddColumn addColumn) {
      this.tableName = addColumn.getTableName();
      this.columns = Collections.singletonList(new Column(addColumn));
    }

    @Data
    @JsonInclude(Include.NON_NULL)
    public static class Column {

      private String name;

      private String type;

      private Constraints constraints;

      public Column(ru.aad.repository.rest.resource.api.AddColumn addColumn) {
        this.name = addColumn.getName();
        this.type = addColumn.getType();
        this.constraints = new Constraints(addColumn.getPrimaryKey(),
            addColumn.getPrimaryKey() == null ? null : "pk_" + addColumn.getTableName(),
            addColumn.getUnique(), addColumn.getNullable());
      }

      @Data
      @AllArgsConstructor
      @JsonInclude(Include.NON_NULL)
      public static class Constraints {

        private Boolean primaryKey;

        private String primaryKeyName;

        private Boolean unique;

        private Boolean nullable;
      }
    }
  }
}
