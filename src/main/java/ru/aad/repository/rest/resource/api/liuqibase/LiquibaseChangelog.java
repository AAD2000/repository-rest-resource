package ru.aad.repository.rest.resource.api.liuqibase;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
@JsonInclude(Include.NON_NULL)
public class LiquibaseChangelog implements Changes {

  private List<LiquibaseChangelogChild> databaseChangeLog;

  public interface LiquibaseChangelogChild {

  }

}
