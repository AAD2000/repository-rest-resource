package ru.aad.repository.rest.resource.repositories;

import org.springframework.data.repository.CrudRepository;
import ru.aad.repository.rest.resource.model.AddColumn;

public interface AddColumnRepository extends CrudRepository<AddColumn, Long> {

}
