package ru.aad.repository.rest.resource.api;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class DataBaseInfo {

  private String url;

  private String username;

  private String password;
}
