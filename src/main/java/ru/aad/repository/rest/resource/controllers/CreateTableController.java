package ru.aad.repository.rest.resource.controllers;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.aad.repository.rest.resource.api.CreateTable;
import ru.aad.repository.rest.resource.services.CreateTableService;

@CrossOrigin
@RestController
@AllArgsConstructor
public class CreateTableController {

  private final CreateTableService createTableService;

  @GetMapping("/create-table")
  public CreateTable getTable(
      @RequestParam(name = "owner") String owner,
      @RequestParam(name = "model") String modelName,
      @RequestParam(name = "name") String tableName) {
    return createTableService.getTable(owner, modelName, tableName);
  }

  @PostMapping("/create-table")
  public void create(@RequestParam(name = "v") Long v, @RequestBody CreateTable entity) {
    createTableService.create(v, entity);
  }

  @DeleteMapping("/create-table/{id}")
  public void delete(@PathVariable(name = "id") Long id) {
    createTableService.delete(id);
  }

  @PutMapping("/create-table/{id}")
  public void update(
      @RequestParam(name = "v") Long v,
      @PathVariable(name = "id") Long id,
      @RequestBody CreateTable entity) {
    createTableService.update(v, id, entity);
  }
}
