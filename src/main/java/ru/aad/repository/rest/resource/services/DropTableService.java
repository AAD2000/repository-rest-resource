package ru.aad.repository.rest.resource.services;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.aad.repository.rest.resource.api.DropTable;
import ru.aad.repository.rest.resource.copiers.Copier;
import ru.aad.repository.rest.resource.mappers.DropTableMapper;
import ru.aad.repository.rest.resource.model.Version;
import ru.aad.repository.rest.resource.repositories.DropTableRepository;
import ru.aad.repository.rest.resource.repositories.VersionRepository;

@Service
@AllArgsConstructor
public class DropTableService {

  private final Copier copier;

  private final DropTableMapper mapper;

  private final VersionRepository versionRepository;

  private final DropTableRepository entityRepository;

  @Transactional
  public void create(Long v, DropTable entity) {
    versionRepository
        .findById(v)
        .orElseThrow(IllegalArgumentException::new)
        .getDropTables()
        .add(mapper.dto2Model(entity));
  }

  public void delete(Long id) {
    entityRepository.deleteById(id);
  }

  @Transactional
  public void update(Long v, Long id, DropTable entity) {
    final Version version =
        versionRepository.findById(v).orElseThrow(IllegalArgumentException::new);

    if (version.isDeployed()) {
      throw new IllegalArgumentException();
    }

    copier.copy(
        entityRepository.findById(id).orElseThrow(IllegalArgumentException::new),
        mapper.dto2Model(entity));
  }
}
