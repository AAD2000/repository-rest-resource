package ru.aad.repository.rest.resource.repositories;

import org.springframework.data.repository.CrudRepository;
import ru.aad.repository.rest.resource.model.TableColumn;

public interface TableColumnRepository extends CrudRepository<TableColumn, Long> {

}
