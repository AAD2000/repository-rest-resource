package ru.aad.repository.rest.resource.repositories;

import java.util.List;
import java.util.Optional;
import org.springframework.data.repository.CrudRepository;
import ru.aad.repository.rest.resource.model.DockerCompose;

public interface DockerComposeRepository extends CrudRepository<DockerCompose, Long> {

  List<DockerCompose> findByOwner(String owner);

  Optional<DockerCompose> findByIdAndOwnerAndModelName(Long id, String owner, String modelName);

  Optional<DockerCompose> findByOwnerAndModelName(String owner, String modelName);

  List<DockerCompose> findAll();
}
