package ru.aad.repository.rest.resource.repositories;

import org.springframework.data.repository.CrudRepository;
import ru.aad.repository.rest.resource.model.DropColumnInfo;

public interface DropColumnInfoRepository extends CrudRepository<DropColumnInfo, Long> {

}
