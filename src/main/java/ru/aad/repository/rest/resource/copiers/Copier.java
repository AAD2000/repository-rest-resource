package ru.aad.repository.rest.resource.copiers;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import lombok.AllArgsConstructor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;
import ru.aad.repository.rest.resource.model.AddColumn;
import ru.aad.repository.rest.resource.model.AddSql;
import ru.aad.repository.rest.resource.model.Connection;
import ru.aad.repository.rest.resource.model.CreateIndex;
import ru.aad.repository.rest.resource.model.CreateIndexColumn;
import ru.aad.repository.rest.resource.model.CreateTable;
import ru.aad.repository.rest.resource.model.DropColumn;
import ru.aad.repository.rest.resource.model.DropColumnInfo;
import ru.aad.repository.rest.resource.model.DropConnection;
import ru.aad.repository.rest.resource.model.DropIndex;
import ru.aad.repository.rest.resource.model.DropTable;
import ru.aad.repository.rest.resource.model.IdInterface;
import ru.aad.repository.rest.resource.model.TableColumn;
import ru.aad.repository.rest.resource.model.Version;
import ru.aad.repository.rest.resource.repositories.AddColumnRepository;
import ru.aad.repository.rest.resource.repositories.AddSqlRepository;
import ru.aad.repository.rest.resource.repositories.ConnectionRepository;
import ru.aad.repository.rest.resource.repositories.CreateIndexColumnRepository;
import ru.aad.repository.rest.resource.repositories.CreateIndexRepository;
import ru.aad.repository.rest.resource.repositories.CreateTableRepository;
import ru.aad.repository.rest.resource.repositories.DropColumnInfoRepository;
import ru.aad.repository.rest.resource.repositories.DropColumnRepository;
import ru.aad.repository.rest.resource.repositories.DropConnectionRepository;
import ru.aad.repository.rest.resource.repositories.DropIndexRepository;
import ru.aad.repository.rest.resource.repositories.DropTableRepository;
import ru.aad.repository.rest.resource.repositories.TableColumnRepository;

@Component
@AllArgsConstructor
public class Copier {

  private final AddColumnRepository addColumnRepository;
  private final AddSqlRepository addSqlRepository;
  private final DropConnectionRepository dropConnectionRepository;
  private final DropIndexRepository dropIndexRepository;
  private final ConnectionRepository connectionRepository;
  private final CreateIndexColumnRepository createIndexColumnRepository;
  private final CreateIndexRepository createIndexRepository;
  private final CreateTableRepository createTableRepository;
  private final DropColumnInfoRepository dropColumnInfoRepository;
  private final DropColumnRepository dropColumnRepository;
  private final DropTableRepository dropTableRepository;
  private final TableColumnRepository tableColumnRepository;

  public void copy(IdInterface model, IdInterface data) {
    if (model instanceof Version && data instanceof Version) {
      this.versionCopy((Version) model, (Version) data);
    }

    if (model instanceof AddColumn && data instanceof AddColumn) {
      this.addColumnCopy((AddColumn) model, (AddColumn) data);
    }

    if (model instanceof AddSql && data instanceof AddSql) {
      this.addSqlCopy((AddSql) model, (AddSql) data);
    }

    if (model instanceof DropConnection && data instanceof DropConnection) {
      this.dropConnectionCopy((DropConnection) model, (DropConnection) data);
    }

    if (model instanceof DropIndex && data instanceof DropIndex) {
      this.dropIndexCopy((DropIndex) model, (DropIndex) data);
    }

    if (model instanceof Connection && data instanceof Connection) {
      this.connectionCopy((Connection) model, (Connection) data);
    }

    if (model instanceof CreateIndex && data instanceof CreateIndex) {
      this.createIndexCopy((CreateIndex) model, (CreateIndex) data);
    }

    if (model instanceof CreateIndexColumn && data instanceof CreateIndexColumn) {
      this.createIndexColumnCopy((CreateIndexColumn) model, (CreateIndexColumn) data);
    }

    if (model instanceof CreateTable && data instanceof CreateTable) {
      this.createTableCopy((CreateTable) model, (CreateTable) data);
    }

    if (model instanceof DropColumn && data instanceof DropColumn) {
      this.dropColumnCopy((DropColumn) model, (DropColumn) data);
    }

    if (model instanceof DropColumnInfo && data instanceof DropColumnInfo) {
      this.dropColumnInfoCopy((DropColumnInfo) model, (DropColumnInfo) data);
    }

    if (model instanceof DropTable && data instanceof DropTable) {
      this.dropTableCopy((DropTable) model, (DropTable) data);
    }

    if (model instanceof TableColumn && data instanceof TableColumn) {
      this.tableColumnCopy((TableColumn) model, (TableColumn) data);
    }
  }

  public void versionCopy(Version version, Version data) {
    version.setVersionName(data.getVersionName());
    version.setDeployed(data.isDeployed());
    idInterfaceCopier(version.getDropIndices(), data.getDropIndices(), dropIndexRepository);
    idInterfaceCopier(
        version.getDropConnections(), data.getDropConnections(), dropConnectionRepository);
    idInterfaceCopier(version.getAddColumns(), data.getAddColumns(), addColumnRepository);
    idInterfaceCopier(version.getConnections(), data.getConnections(), connectionRepository);
    idInterfaceCopier(version.getAddSqls(), data.getAddSqls(), addSqlRepository);
    idInterfaceCopier(version.getCreateIndices(), data.getCreateIndices(), createIndexRepository);
    idInterfaceCopier(version.getCreateTables(), data.getCreateTables(), createTableRepository);
    idInterfaceCopier(version.getDropColumns(), data.getDropColumns(), dropColumnRepository);
    idInterfaceCopier(version.getDropTables(), data.getDropTables(), dropTableRepository);
  }

  public void dropConnectionCopy(DropConnection dropConnection, DropConnection data) {
    dropConnection.setBaseTableName(data.getBaseTableName());
    dropConnection.setName(data.getName());
  }

  public void dropIndexCopy(DropIndex dropIndex, DropIndex data) {
    dropIndex.setName(data.getName());
    dropIndex.setTableName(data.getTableName());
  }

  public void addColumnCopy(AddColumn addColumn, AddColumn data) {
    addColumn.setAutoIncrement(data.getAutoIncrement());
    addColumn.setName(data.getName());
    addColumn.setNullable(data.getNullable());
    addColumn.setPrimaryKey(data.getPrimaryKey());
    addColumn.setTableName(data.getTableName());
    addColumn.setType(data.getType());
    addColumn.setUnique(data.getUnique());
  }

  public void addSqlCopy(AddSql addSql, AddSql data) {
    addSql.setSql(data.getSql());
    addSql.setDdl(data.getDdl());
  }

  public void connectionCopy(Connection connection, Connection data) {
    connection.setBaseTableName(data.getBaseTableName());
    connection.setBaseColumnNames(data.getBaseColumnNames());
    connection.setConstraintName(data.getConstraintName());
    connection.setReferencedTableName(data.getReferencedTableName());
    connection.setReferencedColumnNames(data.getReferencedColumnNames());
  }

  public void createIndexCopy(CreateIndex createIndex, CreateIndex data) {
    createIndex.setUnique(data.getUnique());
    createIndex.setName(data.getName());
    createIndex.setTableName(data.getTableName());
    //    idInterfaceCopier(
    //        createIndex.getCreateIndexColumns(),
    //        data.getCreateIndexColumns(),
    //        createIndexColumnRepository);
  }

  public void createIndexColumnCopy(CreateIndexColumn createIndexColumn, CreateIndexColumn data) {
    createIndexColumn.setName(data.getName());
  }

  public void createTableCopy(CreateTable createTable, CreateTable data) {
    createTable.setName(data.getName());
    //    idInterfaceCopier(createTable.getTableColumns(), data.getTableColumns(),
    // tableColumnRepository);
  }

  public void dropColumnCopy(DropColumn dropColumn, DropColumn data) {
    dropColumn.setTableName(data.getTableName());
    idInterfaceCopier(
        dropColumn.getDropColumnInfos(), data.getDropColumnInfos(), dropColumnInfoRepository);
  }

  public void dropColumnInfoCopy(DropColumnInfo dropColumnInfo, DropColumnInfo data) {
    dropColumnInfo.setName(data.getName());
  }

  public void dropTableCopy(DropTable dropTable, DropTable data) {
    dropTable.setName(data.getName());
    dropTable.setCascade(data.getCascade());
  }

  public void tableColumnCopy(TableColumn tableColumn, TableColumn data) {
    tableColumn.setAutoIncrement(data.getAutoIncrement());
    tableColumn.setNullable(data.getNullable());
    tableColumn.setType(data.getType());
    tableColumn.setName(data.getName());
    tableColumn.setPrimaryKey(data.getPrimaryKey());
    tableColumn.setUnique(data.getUnique());
  }

  private <T extends IdInterface> void idInterfaceCopier(
      List<T> model, List<T> dto, CrudRepository<T, Long> crudRepository) {
    model = model == null ? new ArrayList<>() : model;
    dto = dto == null ? new ArrayList<>() : dto;

    final List<Long> dtoIds = dto.stream().map(IdInterface::getId).collect(Collectors.toList());

    List<T> toDeleteList = new ArrayList<>();
    for (T toDelete : model) {
      if (!dtoIds.contains(toDelete.getId())) {
        crudRepository.findById(toDelete.getId()).ifPresent(toDeleteList::add);
        crudRepository.deleteById(toDelete.getId());
      }
    }

    model.removeAll(toDeleteList);

    List<T> toAddList = new ArrayList<>();

    for (T toAdd : dto) {
      final List<T> collect =
          model.stream()
              .filter(idi -> idi.getId().equals(toAdd.getId()))
              .collect(Collectors.toList());

      if (collect.isEmpty()) {
        toAddList.add(toAdd);
        continue;
      }

      this.copy(collect.get(0), toAdd);
    }

    model.addAll(toAddList);
  }
}
