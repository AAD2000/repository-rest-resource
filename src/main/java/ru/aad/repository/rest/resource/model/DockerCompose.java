package ru.aad.repository.rest.resource.model;

import java.time.LocalDateTime;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import lombok.Data;

@Data
@Entity
@Table(name = "docker_compose")
public class DockerCompose extends IdInterface {

  @Id
  @Column(name = "id")
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Column(name = "created_date")
  private LocalDateTime createdDate;

  @Column(name = "model_name")
  private String modelName;

  @Column(name = "open_port")
  private Long openPort;

  @Column(name = "created")
  private Boolean created;

  @Column(name = "password")
  private String password;

  @Column(name = "user_name")
  private String userName;

  @Column(name = "url")
  private String url;

  @Column(name = "owner")
  private String owner;

  @Column(name = "database")
  private String database;

  @OneToMany(orphanRemoval = true, cascade = CascadeType.ALL)
  @JoinColumn(name = "docker_compose_id")
  private List<Version> versions;
}
