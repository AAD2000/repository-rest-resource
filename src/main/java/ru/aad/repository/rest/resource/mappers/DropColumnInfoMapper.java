package ru.aad.repository.rest.resource.mappers;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface DropColumnInfoMapper {

  ru.aad.repository.rest.resource.api.DropColumnInfo model2Dto(
      ru.aad.repository.rest.resource.model.DropColumnInfo model);

  @Mapping(target = "createdDate", expression = "java(dto.getCreatedDate() == null ? java.time.LocalDateTime.now() : dto.getCreatedDate())")
  ru.aad.repository.rest.resource.model.DropColumnInfo dto2Model(
      ru.aad.repository.rest.resource.api.DropColumnInfo dto);
}
