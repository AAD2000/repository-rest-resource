package ru.aad.repository.rest.resource.helper;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class SideTransactionalHelper {

  @Transactional
  public void execute(Runnable callable){
    callable.run();
  }

}
