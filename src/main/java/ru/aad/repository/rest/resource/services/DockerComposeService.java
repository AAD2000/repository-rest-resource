package ru.aad.repository.rest.resource.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.util.Pair;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import ru.aad.repository.rest.resource.api.AddColumn;
import ru.aad.repository.rest.resource.api.AddSql;
import ru.aad.repository.rest.resource.api.Connection;
import ru.aad.repository.rest.resource.api.CreateIndex;
import ru.aad.repository.rest.resource.api.CreateTable;
import ru.aad.repository.rest.resource.api.DataBaseInfo;
import ru.aad.repository.rest.resource.api.DockerCompose;
import ru.aad.repository.rest.resource.api.DropColumn;
import ru.aad.repository.rest.resource.api.DropConnection;
import ru.aad.repository.rest.resource.api.DropIndex;
import ru.aad.repository.rest.resource.api.DropTable;
import ru.aad.repository.rest.resource.api.EmptyObject;
import ru.aad.repository.rest.resource.api.FileContentDto;
import ru.aad.repository.rest.resource.api.FileData;
import ru.aad.repository.rest.resource.api.liuqibase.LiquibaseAddColumn;
import ru.aad.repository.rest.resource.api.liuqibase.LiquibaseAddSql;
import ru.aad.repository.rest.resource.api.liuqibase.LiquibaseChangelog;
import ru.aad.repository.rest.resource.api.liuqibase.LiquibaseChangelog.LiquibaseChangelogChild;
import ru.aad.repository.rest.resource.api.liuqibase.LiquibaseCreateConnection;
import ru.aad.repository.rest.resource.api.liuqibase.LiquibaseCreateIndex;
import ru.aad.repository.rest.resource.api.liuqibase.LiquibaseCreateTable;
import ru.aad.repository.rest.resource.api.liuqibase.LiquibaseDatabaseTag;
import ru.aad.repository.rest.resource.api.liuqibase.LiquibaseDropColumn;
import ru.aad.repository.rest.resource.api.liuqibase.LiquibaseDropConnection;
import ru.aad.repository.rest.resource.api.liuqibase.LiquibaseDropIndex;
import ru.aad.repository.rest.resource.api.liuqibase.LiquibaseDropTable;
import ru.aad.repository.rest.resource.api.liuqibase.LiquibaseInclude;
import ru.aad.repository.rest.resource.api.liuqibase.LiquibaseInitialTag;
import ru.aad.repository.rest.resource.api.liuqibase.LiquibaseWrapper;
import ru.aad.repository.rest.resource.copiers.Copier;
import ru.aad.repository.rest.resource.helper.FileContent;
import ru.aad.repository.rest.resource.helper.RollbackHelper;
import ru.aad.repository.rest.resource.helper.SideTransactionalHelper;
import ru.aad.repository.rest.resource.mappers.DockerComposeMapper;
import ru.aad.repository.rest.resource.model.Version;
import ru.aad.repository.rest.resource.repositories.DockerComposeRepository;
import ru.aad.repository.rest.resource.repositories.VersionRepository;

@Service
public class DockerComposeService {

  private final String ip;
  private final Copier copier;
  private final ObjectMapper objectMapper;
  private final DockerComposeMapper dockerComposeMapper;
  private final DockerComposeRepository dockerComposeRepository;
  private final VersionRepository versionRepository;
  private final SideTransactionalHelper sideTransactionalHelper;

  @Autowired
  public DockerComposeService(
      @Value("${odd.host-ip}") String ip,
      Copier copier,
      ObjectMapper objectMapper,
      DockerComposeMapper dockerComposeMapper,
      DockerComposeRepository dockerComposeRepository,
      VersionRepository versionRepository,
      SideTransactionalHelper sideTransactionalHelper) {
    this.ip = ip;
    this.copier = copier;
    this.objectMapper = objectMapper;
    this.dockerComposeMapper = dockerComposeMapper;
    this.dockerComposeRepository = dockerComposeRepository;
    this.versionRepository = versionRepository;
    this.sideTransactionalHelper = sideTransactionalHelper;
  }

  public DockerCompose save(DockerCompose dockerCompose) {

    final Optional<ru.aad.repository.rest.resource.model.DockerCompose> byId =
        dockerCompose.getId() == null
            ? Optional.empty()
            : dockerComposeRepository.findById(dockerCompose.getId());

    if (byId.isPresent()) {
      sideTransactionalHelper.execute(() -> editiongDockerCompose(dockerCompose));
      return dockerComposeMapper.model2Dto(
          dockerComposeRepository.findById(dockerCompose.getId()).get());
    }

    return dockerComposeMapper.model2Dto(
        dockerComposeRepository.save(dockerComposeMapper.dto2Model(dockerCompose)));
  }

  private void updateOrCreateVersion(
      ru.aad.repository.rest.resource.model.DockerCompose dockerCompose,
      final Version versionFromApi,
      List<Version> versionsToAdd) {
    final List<Version> foundedVersion =
        dockerCompose.getVersions().stream()
            .filter(v -> v.getId().equals(versionFromApi.getId()))
            .collect(Collectors.toList());

    if (foundedVersion.isEmpty()) {
      versionsToAdd.add(versionFromApi);
      return;
    }

    final Version version = foundedVersion.get(0);

    copier.versionCopy(version, versionFromApi);
  }

  public void editiongDockerCompose(DockerCompose dockerCompose) {
    final ru.aad.repository.rest.resource.model.DockerCompose byId =
        dockerComposeRepository.findById(dockerCompose.getId()).get();

    final ru.aad.repository.rest.resource.model.DockerCompose dockerComposeModel =
        dockerComposeMapper.dto2Model(dockerCompose);

    List<Version> versionsToAdd = new ArrayList<>();

    if (dockerComposeModel.getVersions() != null) {
      for (Version v : dockerComposeModel.getVersions()) {
        if (isVersionDeployed(byId, v)) {
          continue;
        }

        updateOrCreateVersion(byId, v, versionsToAdd);
      }
    }

    final List<Long> versionIdsFromDb =
        byId.getVersions().stream().map(Version::getId).collect(Collectors.toList());

    List<Version> versionsToDelete = new ArrayList<>();

    for (Long versionId : versionIdsFromDb) {
      if (dockerComposeModel.getVersions().stream()
          .map(Version::getId)
          .noneMatch(versionId::equals)) {
        versionRepository.findById(versionId).ifPresent(versionsToDelete::add);
        versionRepository.deleteById(versionId);
      }
    }

    byId.getVersions().removeAll(versionsToDelete);

    byId.getVersions().addAll(versionsToAdd);
  }

  public boolean isVersionDeployed(
      ru.aad.repository.rest.resource.model.DockerCompose dockerCompose, Version v) {
    final List<Version> versions =
        dockerCompose.getVersions().stream()
            .filter(version -> version.getId().equals(v.getId()))
            .collect(Collectors.toList());

    if (versions.isEmpty()) {
      return false;
    }

    return versions.get(0).isDeployed();
  }

  public List<DockerCompose> getDockerComposes(String owner) {
    return dockerComposeRepository.findByOwner(owner).stream()
        .map(dockerComposeMapper::model2Dto)
        .collect(Collectors.toList());
  }

  @Transactional
  public DockerCompose getDockerComposes(String owner, String model) {
    return dockerComposeRepository
        .findByOwnerAndModelName(owner, model)
        .map(dockerComposeMapper::model2Dto)
        .get();
  }

  public boolean delete(Long id) {
    dockerComposeRepository.deleteById(id);
    return true;
  }

  @SneakyThrows
  @Transactional(readOnly = true)
  public FileContentDto getFileData(
      Long id, String owner, String model, boolean rollback, String tag, boolean force) {
    System.out.println(id + " " + owner + " " + model + " " + rollback + " " + tag);

    final DockerCompose dockerCompose =
        dockerComposeMapper.model2Dto(
            dockerComposeRepository
                .findByIdAndOwnerAndModelName(id, owner, model)
                .orElseThrow(IllegalAccessError::new));

    final Pair<Boolean, List<String>> problems =
        RollbackHelper.getProblems(dockerCompose.getVersions().stream(), tag);

    if (rollback && !force && !problems.getSecond().isEmpty() && !problems.getFirst()) {
      return FileContentDto.builder()
          .containsDdlSql(problems.getFirst())
          .problems(problems.getSecond())
          .build();
    }

    List<FileData> fileDataList = new ArrayList<>();

    FileData dockerComposeFile =
        FileData.builder()
            .content(
                rollback
                    ? FileContent.dockerComposeRollback(dockerCompose, tag)
                    : FileContent.dockerComposeUpdate(dockerCompose))
            .path(String.format("../projects/%s/%s/docker-compose.yml", owner, model))
            .build();

    fileDataList.add(dockerComposeFile);

    FileData liquibasePropertiesFile =
        FileData.builder()
            .content(FileContent.liquibaseProperties(dockerCompose, ip))
            .path(String.format("../projects/%s/%s/changelog/liquibase.properties", owner, model))
            .build();

    fileDataList.add(liquibasePropertiesFile);

    List<LiquibaseChangelogChild> liquibaseChangelogChildList = new ArrayList<>();

    liquibaseChangelogChildList.add(new LiquibaseInitialTag());

    for (ru.aad.repository.rest.resource.api.Version v : dockerCompose.getVersions()) {
      LiquibaseInclude liquibaseInclude =
          new LiquibaseInclude("/" + v.getVersionName() + "/changelog.json");
      liquibaseChangelogChildList.add(liquibaseInclude);
    }

    LiquibaseChangelog liquibaseChangelog = new LiquibaseChangelog(liquibaseChangelogChildList);

    FileData changelogMaster =
        FileData.builder()
            .path(String.format("../projects/%s/%s/changelog/changelog-master.json", owner, model))
            .content(objectMapper.writeValueAsString(liquibaseChangelog))
            .build();

    fileDataList.add(changelogMaster);

    for (ru.aad.repository.rest.resource.api.Version v : dockerCompose.getVersions()) {
      List<LiquibaseChangelogChild> liquibaseChangelogVersionChildren = new ArrayList<>();

      if (!CollectionUtils.isEmpty(v.getCreateTables())) {

        liquibaseChangelogVersionChildren.addAll(
            addVersionChilds(
                v.getCreateTables(),
                fileDataList,
                String.format("../projects/%s/%s/changelog/%s/", owner, model, v.getVersionName()),
                new LiquibaseDatabaseTag(v)));
      }

      if (!CollectionUtils.isEmpty(v.getAddColumns())) {

        liquibaseChangelogVersionChildren.addAll(
            addVersionChilds(
                v.getAddColumns(),
                fileDataList,
                String.format("../projects/%s/%s/changelog/%s/", owner, model, v.getVersionName()),
                new LiquibaseDatabaseTag(v)));
      }

      if (!CollectionUtils.isEmpty(v.getAddSqls())) {

        liquibaseChangelogVersionChildren.addAll(
            addVersionChilds(
                v.getAddSqls(),
                fileDataList,
                String.format("../projects/%s/%s/changelog/%s/", owner, model, v.getVersionName()),
                new LiquibaseDatabaseTag(v)));
      }

      if (!CollectionUtils.isEmpty(v.getCreateIndices())) {

        liquibaseChangelogVersionChildren.addAll(
            addVersionChilds(
                v.getCreateIndices(),
                fileDataList,
                String.format("../projects/%s/%s/changelog/%s/", owner, model, v.getVersionName()),
                new LiquibaseDatabaseTag(v)));
      }

      if (!CollectionUtils.isEmpty(v.getConnections())) {

        liquibaseChangelogVersionChildren.addAll(
            addVersionChilds(
                v.getConnections(),
                fileDataList,
                String.format("../projects/%s/%s/changelog/%s/", owner, model, v.getVersionName()),
                new LiquibaseDatabaseTag(v)));
      }

      if (!CollectionUtils.isEmpty(v.getDropConnections())) {

        liquibaseChangelogVersionChildren.addAll(
            addVersionChilds(
                v.getDropConnections(),
                fileDataList,
                String.format("../projects/%s/%s/changelog/%s/", owner, model, v.getVersionName()),
                new LiquibaseDatabaseTag(v)));
      }

      if (!CollectionUtils.isEmpty(v.getDropIndices())) {

        liquibaseChangelogVersionChildren.addAll(
            addVersionChilds(
                v.getDropIndices(),
                fileDataList,
                String.format("../projects/%s/%s/changelog/%s/", owner, model, v.getVersionName()),
                new LiquibaseDatabaseTag(v)));
      }

      if (!CollectionUtils.isEmpty(v.getDropColumns())) {

        liquibaseChangelogVersionChildren.addAll(
            addVersionChilds(
                v.getDropColumns(),
                fileDataList,
                String.format("../projects/%s/%s/changelog/%s/", owner, model, v.getVersionName()),
                new LiquibaseDatabaseTag(v)));
      }

      if (!CollectionUtils.isEmpty(v.getDropTables())) {

        liquibaseChangelogVersionChildren.addAll(
            addVersionChilds(
                v.getDropTables(),
                fileDataList,
                String.format("../projects/%s/%s/changelog/%s/", owner, model, v.getVersionName()),
                new LiquibaseDatabaseTag(v)));
      }

      LiquibaseChangelog liquibaseChangelogVersion =
          new LiquibaseChangelog(liquibaseChangelogVersionChildren);

      fileDataList.add(
          FileData.builder()
              .path(
                  String.format(
                      "../projects/%s/%s/changelog/%s/changelog.json",
                      owner, model, v.getVersionName()))
              .content(objectMapper.writeValueAsString(liquibaseChangelogVersion))
              .build());
    }

    final Optional<ru.aad.repository.rest.resource.api.Version> max =
        dockerCompose.getVersions().stream()
            .max(
                (version, t1) -> {
                  final String[] split = version.getVersionName().split("\\.");
                  final String[] split1 = t1.getVersionName().split("\\.");

                  if (Integer.parseInt(split[0]) > Integer.parseInt(split1[0])) {
                    return 1;
                  }

                  if (Integer.parseInt(split[0]) == Integer.parseInt(split1[0])
                      && Integer.parseInt(split[1]) > Integer.parseInt(split1[1])) {
                    return 1;
                  }

                  if (Integer.parseInt(split[0]) == Integer.parseInt(split1[0])
                      && Integer.parseInt(split[1]) == Integer.parseInt(split1[1])) {
                    return 0;
                  }

                  return -1;
                });

    return FileContentDto.builder()
        .fileDataList(fileDataList)
        .foreign(dockerCompose.getUrl() != null)
        .version(tag == null ? max.isPresent() ? max.get().getVersionName() : "0.0" : tag)
        .build();
  }

  @SneakyThrows
  private <T> List<LiquibaseChangelogChild> addVersionChilds(
      List<T> childs,
      List<FileData> fileDataList,
      String subpath,
      LiquibaseDatabaseTag liquibaseDatabaseTag) {
    List<LiquibaseChangelogChild> liquibaseChangelogChildList = new ArrayList<>();

    for (T c : childs) {
      if (c instanceof CreateTable) {
        CreateTable createTable = (CreateTable) c;

        String fileName =
            createTable.getName() + "_" + createTable.getCreatedDate().toString() + ".json";

        liquibaseChangelogChildList.add(new LiquibaseInclude("tables/" + fileName));

        LiquibaseCreateTable liquibaseCreateTable = new LiquibaseCreateTable(createTable);

        final LiquibaseWrapper liquibaseWrapper =
            new LiquibaseWrapper(
                "create_table_" + createTable.getName() + "_" + createTable.getCreatedDate(),
                liquibaseCreateTable,
                liquibaseDatabaseTag,
                new LiquibaseDropTable(liquibaseCreateTable));

        FileData fileData =
            FileData.builder()
                .content(objectMapper.writeValueAsString(liquibaseWrapper))
                .path(subpath + "tables/" + fileName)
                .build();

        fileDataList.add(fileData);
      }

      if (c instanceof AddColumn) {
        AddColumn addColumn = (AddColumn) c;

        String fileName =
            addColumn.getName() + "_" + addColumn.getCreatedDate().toString() + ".json";

        liquibaseChangelogChildList.add(new LiquibaseInclude("add_column/" + fileName));

        LiquibaseAddColumn liquibaseAddColumn = new LiquibaseAddColumn(addColumn);

        final LiquibaseWrapper liquibaseWrapper =
            new LiquibaseWrapper(
                "add_column_" + addColumn.getName() + "_" + addColumn.getCreatedDate(),
                liquibaseAddColumn,
                liquibaseDatabaseTag,
                new LiquibaseDropColumn(liquibaseAddColumn));

        FileData fileData =
            FileData.builder()
                .content(objectMapper.writeValueAsString(liquibaseWrapper))
                .path(subpath + "add_column/" + fileName)
                .build();

        fileDataList.add(fileData);
      }

      if (c instanceof Connection) {
        Connection connection = (Connection) c;

        String fileName =
            connection.getConstraintName() + "_" + connection.getCreatedDate().toString() + ".json";

        liquibaseChangelogChildList.add(new LiquibaseInclude("connections/" + fileName));

        LiquibaseCreateConnection liquibaseCreateConnection =
            new LiquibaseCreateConnection(connection);

        final LiquibaseWrapper liquibaseWrapper =
            new LiquibaseWrapper(
                "add_connection_"
                    + connection.getConstraintName()
                    + "_"
                    + connection.getCreatedDate(),
                liquibaseCreateConnection,
                liquibaseDatabaseTag,
                new LiquibaseDropConnection(liquibaseCreateConnection));

        FileData fileData =
            FileData.builder()
                .content(objectMapper.writeValueAsString(liquibaseWrapper))
                .path(subpath + "connections/" + fileName)
                .build();

        fileDataList.add(fileData);
      }

      if (c instanceof CreateIndex) {
        CreateIndex createIndex = (CreateIndex) c;

        String fileName =
            createIndex.getName() + "_" + createIndex.getCreatedDate().toString() + ".json";

        liquibaseChangelogChildList.add(new LiquibaseInclude("indexes/" + fileName));

        LiquibaseCreateIndex liquibaseCreateIndex = new LiquibaseCreateIndex(createIndex);

        final LiquibaseWrapper liquibaseWrapper =
            new LiquibaseWrapper(
                "create_index_" + createIndex.getName() + "_" + createIndex.getCreatedDate(),
                liquibaseCreateIndex,
                liquibaseDatabaseTag,
                new LiquibaseDropIndex(liquibaseCreateIndex));

        FileData fileData =
            FileData.builder()
                .content(objectMapper.writeValueAsString(liquibaseWrapper))
                .path(subpath + "indexes/" + fileName)
                .build();

        fileDataList.add(fileData);
      }

      if (c instanceof AddSql) {
        AddSql addSql = (AddSql) c;

        String fileName = addSql.getId() + "_" + addSql.getCreatedDate().toString() + ".json";

        liquibaseChangelogChildList.add(new LiquibaseInclude("sqls/" + fileName));

        LiquibaseAddSql liquibaseAddSql = new LiquibaseAddSql(addSql);

        final LiquibaseWrapper liquibaseWrapper =
            new LiquibaseWrapper(
                "add_sql_" + addSql.getId() + "_" + addSql.getCreatedDate(),
                liquibaseAddSql,
                liquibaseDatabaseTag,
                new EmptyObject());

        FileData fileData =
            FileData.builder()
                .content(objectMapper.writeValueAsString(liquibaseWrapper))
                .path(subpath + "sqls/" + fileName)
                .build();

        fileDataList.add(fileData);
      }

      if (c instanceof DropIndex) {
        DropIndex dropIndex = (DropIndex) c;

        String fileName =
            dropIndex.getName() + "_" + dropIndex.getCreatedDate().toString() + ".json";

        liquibaseChangelogChildList.add(new LiquibaseInclude("dropindex/" + fileName));

        LiquibaseDropIndex liquibaseDropIndex = new LiquibaseDropIndex(dropIndex);

        final LiquibaseWrapper liquibaseWrapper =
            new LiquibaseWrapper(
                "drop_index_" + dropIndex.getName() + "_" + dropIndex.getCreatedDate(),
                liquibaseDropIndex,
                liquibaseDatabaseTag,
                new EmptyObject());

        FileData fileData =
            FileData.builder()
                .content(objectMapper.writeValueAsString(liquibaseWrapper))
                .path(subpath + "dropindex/" + fileName)
                .build();

        fileDataList.add(fileData);
      }

      if (c instanceof DropConnection) {
        DropConnection dropConnection = (DropConnection) c;

        String fileName =
            dropConnection.getName() + "_" + dropConnection.getCreatedDate().toString() + ".json";

        liquibaseChangelogChildList.add(new LiquibaseInclude("dropconnection/" + fileName));

        LiquibaseDropConnection liquibaseDropConnection =
            new LiquibaseDropConnection(dropConnection);

        final LiquibaseWrapper liquibaseWrapper =
            new LiquibaseWrapper(
                "drop_connection_" + dropConnection.getId() + "_" + dropConnection.getCreatedDate(),
                liquibaseDropConnection,
                liquibaseDatabaseTag,
                new EmptyObject());

        FileData fileData =
            FileData.builder()
                .content(objectMapper.writeValueAsString(liquibaseWrapper))
                .path(subpath + "dropconnection/" + fileName)
                .build();

        fileDataList.add(fileData);
      }

      if (c instanceof DropColumn) {
        DropColumn dropColumn = (DropColumn) c;

        String fileName =
            dropColumn.getTableName() + "_" + dropColumn.getCreatedDate().toString() + ".json";

        liquibaseChangelogChildList.add(new LiquibaseInclude("dropcolumns/" + fileName));

        LiquibaseDropColumn liquibaseDropColumn = new LiquibaseDropColumn(dropColumn);

        final LiquibaseWrapper liquibaseWrapper =
            new LiquibaseWrapper(
                "drop_column_" + dropColumn.getId() + "_" + dropColumn.getCreatedDate(),
                liquibaseDropColumn,
                liquibaseDatabaseTag,
                new EmptyObject());

        FileData fileData =
            FileData.builder()
                .content(objectMapper.writeValueAsString(liquibaseWrapper))
                .path(subpath + "dropcolumns/" + fileName)
                .build();

        fileDataList.add(fileData);
      }

      if (c instanceof DropTable) {
        DropTable dropTable = (DropTable) c;

        String fileName =
            dropTable.getName() + "_" + dropTable.getCreatedDate().toString() + ".json";

        liquibaseChangelogChildList.add(new LiquibaseInclude("droptable/" + fileName));

        LiquibaseDropTable liquibaseDropTable = new LiquibaseDropTable(dropTable);

        final LiquibaseWrapper liquibaseWrapper =
            new LiquibaseWrapper(
                "drop_table_" + dropTable.getName() + dropTable.getCreatedDate(),
                liquibaseDropTable,
                liquibaseDatabaseTag,
                new EmptyObject());

        FileData fileData =
            FileData.builder()
                .content(objectMapper.writeValueAsString(liquibaseWrapper))
                .path(subpath + "droptable/" + fileName)
                .build();

        fileDataList.add(fileData);
      }
    }

    return liquibaseChangelogChildList;
  }

  @Transactional
  public boolean afterDeploy(Long id, String owner, String model, boolean rollback, String tag) {
    ru.aad.repository.rest.resource.model.DockerCompose dockerCompose =
        dockerComposeRepository.findByIdAndOwnerAndModelName(id, owner, model).orElse(null);

    if (dockerCompose == null) {
      return false;
    }

    for (Version v : dockerCompose.getVersions()) {
      if (rollback && tag != null) {
        final String[] split = v.getVersionName().split("\\.");
        final String[] split1 = tag.split("\\.");

        if (Integer.parseInt(split[0]) > Integer.parseInt(split1[0])) {
          v.setDeployed(false);
        }

        if (Integer.parseInt(split[0]) == Integer.parseInt(split1[0])
            && Integer.parseInt(split[1]) > Integer.parseInt(split1[1])) {
          v.setDeployed(false);
        }
      }
      if (!rollback) {
        v.setDeployed(true);
      }
    }

    return true;
  }

  public DataBaseInfo dataBaseInfo(Long id) {
    final ru.aad.repository.rest.resource.model.DockerCompose dockerCompose =
        dockerComposeRepository.findById(id).orElseThrow(IllegalArgumentException::new);

    return DataBaseInfo.builder()
        .password(dockerCompose.getPassword())
        .username(dockerCompose.getUserName())
        .url(
            dockerCompose.getUrl() != null
                ? dockerCompose.getUrl()
                : String.format(
                    FileContent.getJdbcPreUrl(dockerCompose.getDatabase()) + "%s:%s/%s",
                    ip,
                    dockerCompose.getOpenPort(),
                    dockerCompose.getModelName()))
        .build();
  }

  public DockerCompose getDockerCompose(Long id) {
    final DockerCompose dockerCompose =
        dockerComposeRepository.findById(id).map(dockerComposeMapper::model2Dto).orElse(null);

    dockerCompose
        .getVersions()
        .sort(
            (version, t1) -> {
              final String[] split = version.getVersionName().split("\\.");
              final String[] t1Split = t1.getVersionName().split("\\.");

              Integer v1Major = Integer.parseInt(split[0]);
              Integer v1Minor = Integer.parseInt(split[1]);

              Integer v2Major = Integer.parseInt(t1Split[0]);
              Integer v2Minor = Integer.parseInt(t1Split[1]);

              final int i = v1Major.compareTo(v2Major);

              if (i == 0) {
                return v1Minor.compareTo(v2Minor);
              }

              return i;
            });

    return dockerCompose;
  }

  public List<Long> usedPorts() {
    return dockerComposeRepository.findAll().stream()
        .map(ru.aad.repository.rest.resource.model.DockerCompose::getOpenPort)
        .collect(Collectors.toList());
  }
}
