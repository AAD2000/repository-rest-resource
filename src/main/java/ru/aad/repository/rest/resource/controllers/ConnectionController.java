package ru.aad.repository.rest.resource.controllers;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.aad.repository.rest.resource.api.Connection;
import ru.aad.repository.rest.resource.services.ConnectionService;

@CrossOrigin
@RestController
@AllArgsConstructor
public class ConnectionController {

  private final ConnectionService connectionService;

  @PostMapping("/connection")
  public void create(@RequestParam(name = "v") Long v, @RequestBody Connection entity) {
    connectionService.create(v, entity);
  }

  @DeleteMapping("/connection/{id}")
  public void delete(@PathVariable(name = "id") Long id) {
    connectionService.delete(id);
  }

  @PutMapping("/connection/{id}")
  public void update(
      @RequestParam(name = "v") Long v,
      @PathVariable(name = "id") Long id,
      @RequestBody Connection entity) {
    connectionService.update(v, id, entity);
  }
}
