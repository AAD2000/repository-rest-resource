package ru.aad.repository.rest.resource.controllers;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.aad.repository.rest.resource.api.AddColumn;
import ru.aad.repository.rest.resource.services.AddColumnService;

@CrossOrigin
@RestController
@AllArgsConstructor
public class AddColumnController {

  private final AddColumnService addColumnService;

  @PostMapping("/add-column")
  public void create(@RequestParam(name = "v") Long v, @RequestBody AddColumn entity) {
    addColumnService.create(v, entity);
  }

  @DeleteMapping("/add-column/{id}")
  public void delete(@PathVariable(name = "id") Long id) {
    addColumnService.delete(id);
  }

  @PutMapping("/add-column/{id}")
  public void update(
      @RequestParam(name = "v") Long v,
      @PathVariable(name = "id") Long id,
      @RequestBody AddColumn addColumn) {
    addColumnService.update(v, id, addColumn);
  }
}
