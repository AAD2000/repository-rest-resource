package ru.aad.repository.rest.resource.services;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.aad.repository.rest.resource.api.Version;
import ru.aad.repository.rest.resource.mappers.VersionMapper;
import ru.aad.repository.rest.resource.repositories.DockerComposeRepository;
import ru.aad.repository.rest.resource.repositories.VersionRepository;

@Service
@AllArgsConstructor
public class VersionService {
  private final VersionRepository versionRepository;
  private final DockerComposeRepository dockerComposeRepository;
  private final VersionMapper versionMapper;

  public void delete(Long id) {
    versionRepository.deleteById(id);
  }

  @Transactional
  public void create(Long dc, Version version) {
    ru.aad.repository.rest.resource.model.DockerCompose byId =
        dockerComposeRepository.findById(dc).orElseThrow(IllegalArgumentException::new);

    final ru.aad.repository.rest.resource.model.Version version1 = versionMapper.dto2Model(version);
    byId.getVersions().add(version1);
  }

  @Transactional
  public Version get(Long id) {
    return versionRepository.findById(id).map(versionMapper::model2Dto).get();
  }
}
