package ru.aad.repository.rest.resource.mappers;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface DropTableMapper {

  ru.aad.repository.rest.resource.api.DropTable model2Dto(
      ru.aad.repository.rest.resource.model.DropTable model);

  @Mapping(target = "createdDate", expression = "java(dto.getCreatedDate() == null ? java.time.LocalDateTime.now() : dto.getCreatedDate())")
  ru.aad.repository.rest.resource.model.DropTable dto2Model(
      ru.aad.repository.rest.resource.api.DropTable dto);
}
