package ru.aad.repository.rest.resource.controllers;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.aad.repository.rest.resource.api.DropIndex;
import ru.aad.repository.rest.resource.services.DropIndexService;

@CrossOrigin
@RestController
@AllArgsConstructor
public class DropIndexController {

  private final DropIndexService dropIndexService;

  @PostMapping("/drop-index")
  public void create(@RequestParam(name = "v") Long v, @RequestBody DropIndex entity) {
    dropIndexService.create(v, entity);
  }

  @DeleteMapping("/drop-index/{id}")
  public void delete(@PathVariable(name = "id") Long id) {
    dropIndexService.delete(id);
  }

  @PutMapping("/drop-index/{id}")
  public void update(
      @RequestParam(name = "v") Long v,
      @PathVariable(name = "id") Long id,
      @RequestBody DropIndex entity) {
    dropIndexService.update(v, id, entity);
  }
}
