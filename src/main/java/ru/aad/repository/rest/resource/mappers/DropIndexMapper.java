package ru.aad.repository.rest.resource.mappers;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface DropIndexMapper {

  ru.aad.repository.rest.resource.api.DropIndex model2Dto(
      ru.aad.repository.rest.resource.model.DropIndex model);

  @Mapping(target = "createdDate", expression = "java(dto.getCreatedDate() == null ? java.time.LocalDateTime.now() : dto.getCreatedDate())")
  ru.aad.repository.rest.resource.model.DropIndex dto2Model(
      ru.aad.repository.rest.resource.api.DropIndex dto);
}
