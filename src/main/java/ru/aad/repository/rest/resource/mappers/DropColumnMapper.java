package ru.aad.repository.rest.resource.mappers;

import java.util.ArrayList;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

@Mapper(componentModel = "spring", uses = DropColumnInfoMapper.class)
public interface DropColumnMapper {

  ru.aad.repository.rest.resource.api.DropColumn model2Dto(
      ru.aad.repository.rest.resource.model.DropColumn model);

  @AfterMapping
  default void afterModel2Dto(@MappingTarget ru.aad.repository.rest.resource.api.DropColumn dto) {
    dto.setDropColumnInfos(dto.getDropColumnInfos() == null ? new ArrayList<>() : dto.getDropColumnInfos());
  }

  @Mapping(target = "createdDate", expression = "java(dto.getCreatedDate() == null ? java.time.LocalDateTime.now() : dto.getCreatedDate())")
  ru.aad.repository.rest.resource.model.DropColumn dto2Model(
      ru.aad.repository.rest.resource.api.DropColumn dto);
}
