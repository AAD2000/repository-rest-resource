package ru.aad.repository.rest.resource.controllers;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.aad.repository.rest.resource.api.CreateIndex;
import ru.aad.repository.rest.resource.services.CreateIndexService;

@CrossOrigin
@RestController
@AllArgsConstructor
public class CreateIndexController {

  private final CreateIndexService createIndexService;

  @PostMapping("/create-index")
  public void create(@RequestParam(name = "v") Long v, @RequestBody CreateIndex entity) {
    createIndexService.create(v, entity);
  }

  @DeleteMapping("/create-index/{id}")
  public void delete(@PathVariable(name = "id") Long id) {
    createIndexService.delete(id);
  }

  @PutMapping("/create-index/{id}")
  public void update(
      @RequestParam(name = "v") Long v,
      @PathVariable(name = "id") Long id,
      @RequestBody CreateIndex entity) {
    createIndexService.update(v, id, entity);
  }

  @GetMapping("/create-index/{id}")
  public CreateIndex get(@PathVariable(name = "id") Long id) {
    return createIndexService.get(id);
  }
}
