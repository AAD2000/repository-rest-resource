package ru.aad.repository.rest.resource.services;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.aad.repository.rest.resource.api.AddSql;
import ru.aad.repository.rest.resource.copiers.Copier;
import ru.aad.repository.rest.resource.mappers.AddSqlMapper;
import ru.aad.repository.rest.resource.model.Version;
import ru.aad.repository.rest.resource.repositories.AddSqlRepository;
import ru.aad.repository.rest.resource.repositories.VersionRepository;

@Service
@AllArgsConstructor
public class AddSqlService {

  private final Copier copier;

  private final AddSqlMapper mapper;

  private final VersionRepository versionRepository;

  private final AddSqlRepository entityRepository;

  @Transactional
  public void create(Long v, AddSql entity) {
    versionRepository
        .findById(v)
        .orElseThrow(IllegalArgumentException::new)
        .getAddSqls()
        .add(mapper.dto2Model(entity));
  }

  public void delete(Long id) {
    entityRepository.deleteById(id);
  }

  @Transactional
  public void update(Long v, Long id, AddSql entity) {
    final Version version =
        versionRepository.findById(v).orElseThrow(IllegalArgumentException::new);

    if (version.isDeployed()) {
      throw new IllegalArgumentException();
    }

    copier.copy(
        entityRepository.findById(id).orElseThrow(IllegalArgumentException::new),
        mapper.dto2Model(entity));
  }
}
