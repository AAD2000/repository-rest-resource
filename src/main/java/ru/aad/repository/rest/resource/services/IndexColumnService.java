package ru.aad.repository.rest.resource.services;

import java.util.List;
import java.util.stream.Collectors;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.aad.repository.rest.resource.api.CreateIndexColumn;
import ru.aad.repository.rest.resource.copiers.Copier;
import ru.aad.repository.rest.resource.mappers.CreateIndexColumnMapper;
import ru.aad.repository.rest.resource.model.CreateIndex;
import ru.aad.repository.rest.resource.repositories.CreateIndexColumnRepository;
import ru.aad.repository.rest.resource.repositories.CreateIndexRepository;
import ru.aad.repository.rest.resource.repositories.VersionRepository;

@Service
@AllArgsConstructor
public class IndexColumnService {

  private final Copier copier;

  private final CreateIndexColumnRepository tableColumnRepository;

  private final VersionRepository versionRepository;

  private final CreateIndexColumnMapper tableColumnMapper;

  private final CreateIndexRepository createTableRepository;

  public List<CreateIndexColumn> getTableColumns(Long ct) {
    return createTableRepository
        .findById(ct)
        .orElseThrow(IllegalArgumentException::new)
        .getCreateIndexColumns()
        .stream()
        .map(tableColumnMapper::model2Dto)
        .collect(Collectors.toList());
  }

  public Long create(Long v, Long ct, CreateIndexColumn entity) {
    if (versionRepository.findById(v).orElseThrow(IllegalArgumentException::new).isDeployed()) {
      throw new IllegalArgumentException();
    }

    final CreateIndex createTable =
        createTableRepository.findById(ct).orElseThrow(IllegalArgumentException::new);

    final ru.aad.repository.rest.resource.model.CreateIndexColumn tableColumn =
        tableColumnMapper.dto2Model(entity);

    tableColumn.setCreateIndex(createTable);

    return tableColumnRepository.save(tableColumn).getId();
  }

  public void delete(Long id) {
    tableColumnRepository.deleteById(id);
  }

  @Transactional
  public void update(Long v, Long ct, Long id, CreateIndexColumn entity) {
    if (versionRepository.findById(v).orElseThrow(IllegalArgumentException::new).isDeployed()) {
      throw new IllegalArgumentException();
    }

    final CreateIndex createTable =
        createTableRepository.findById(ct).orElseThrow(IllegalArgumentException::new);

    copier.copy(
        tableColumnRepository.findById(id).orElseThrow(IllegalArgumentException::new),
        tableColumnMapper.dto2Model(entity));
  }
}
