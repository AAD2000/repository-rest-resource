package ru.aad.repository.rest.resource.services;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.aad.repository.rest.resource.api.CreateTable;
import ru.aad.repository.rest.resource.copiers.Copier;
import ru.aad.repository.rest.resource.mappers.CreateTableMapper;
import ru.aad.repository.rest.resource.model.Version;
import ru.aad.repository.rest.resource.repositories.CreateTableRepository;
import ru.aad.repository.rest.resource.repositories.VersionRepository;

@Service
@AllArgsConstructor
public class CreateTableService {

  private final Copier copier;

  private final CreateTableMapper mapper;

  private final VersionRepository versionRepository;

  private final CreateTableRepository entityRepository;

  public CreateTable getTable(String owner, String modelName, String tableName) {
    return mapper.model2Dto(
        entityRepository.findByNameModelOwner(modelName, owner, tableName).orElse(null));
  }

  @Transactional
  public void create(Long v, CreateTable entity) {
    versionRepository
        .findById(v)
        .orElseThrow(IllegalArgumentException::new)
        .getCreateTables()
        .add(mapper.dto2Model(entity));
  }

  public void delete(Long id) {
    entityRepository.deleteById(id);
  }

  @Transactional
  public void update(Long v, Long id, CreateTable entity) {
    final Version version =
        versionRepository.findById(v).orElseThrow(IllegalArgumentException::new);

    if (version.isDeployed()) {
      throw new IllegalArgumentException();
    }

    copier.copy(
        entityRepository.findById(id).orElseThrow(IllegalArgumentException::new),
        mapper.dto2Model(entity));
  }
}
