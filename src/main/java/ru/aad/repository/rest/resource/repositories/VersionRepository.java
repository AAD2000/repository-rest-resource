package ru.aad.repository.rest.resource.repositories;

import org.springframework.data.repository.CrudRepository;
import ru.aad.repository.rest.resource.model.Version;

public interface VersionRepository  extends CrudRepository<Version, Long> {

}
