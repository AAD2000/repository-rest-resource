package ru.aad.repository.rest.resource.api.liuqibase;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import java.util.ArrayList;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import ru.aad.repository.rest.resource.api.CreateIndexColumn;

@Data
@JsonInclude(Include.NON_NULL)
public class LiquibaseCreateIndex implements Changes {

  private CreateIndex createIndex;


  public LiquibaseCreateIndex(ru.aad.repository.rest.resource.api.CreateIndex createIndex) {
    this.createIndex = new CreateIndex(createIndex);
  }

  @Data
  @JsonInclude(Include.NON_NULL)
  public static class CreateIndex {

    private Boolean unique;

    private String tableName;

    private String indexName;

    private List<Column> columns;

    public CreateIndex(ru.aad.repository.rest.resource.api.CreateIndex createIndex) {
      this.unique = createIndex.getUnique();
      this.tableName = createIndex.getTableName();
      this.indexName = createIndex.getName();
      this.columns = new ArrayList<>();

      for (CreateIndexColumn c : createIndex.getCreateIndexColumns()) {
        columns.add(new Column(c.getName()));
      }
    }

    @Data
    @AllArgsConstructor
    @JsonInclude(Include.NON_NULL)
    public static class Column {

      private String name;
    }
  }
}
