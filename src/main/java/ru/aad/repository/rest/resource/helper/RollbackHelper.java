package ru.aad.repository.rest.resource.helper;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import lombok.experimental.UtilityClass;
import org.springframework.data.util.Pair;
import ru.aad.repository.rest.resource.api.Version;

@UtilityClass
public class RollbackHelper {

  public Pair<Boolean, List<String>> getProblems(Stream<Version> versionStream, String tag) {

    if (tag == null) {
      return Pair.of(false, new ArrayList<>());
    }

    Map<String, Boolean> stringBooleanMap = new HashMap<>();

    final Boolean[] addSqContainsDdl = {false};

    versionStream
        .filter(v -> v.getVersionName().compareTo(tag) == 1)
        .sorted(Comparator.comparing(Version::getVersionName))
        .forEach(
            v -> {
              v.getCreateTables().forEach(ct -> stringBooleanMap.put(ct.getName(), true));

              v.getAddColumns()
                  .forEach(
                      ac -> {
                        stringBooleanMap.put(ac.getName() + "{in}" + ac.getTableName(), true);
                      });

              v.getDropTables()
                  .forEach(
                      dt -> {
                        if (stringBooleanMap.containsKey(dt.getName())) {
                          stringBooleanMap.put(dt.getName(), false);
                        }
                      });

              v.getDropColumns()
                  .forEach(
                      dc -> {
                        dc.getDropColumnInfos()
                            .forEach(
                                dci -> {
                                  if (stringBooleanMap.containsKey(
                                      dci.getName() + "{in}" + dc.getTableName())) {
                                    stringBooleanMap.put(
                                        dci.getName() + "{in}" + dc.getTableName(), false);
                                  }
                                });
                      });

              v.getAddSqls()
                  .forEach(
                      as -> {
                        if (as.getDdl()) {
                          addSqContainsDdl[0] = true;
                        }
                      });
            });

    return Pair.of(
        addSqContainsDdl[0],
        stringBooleanMap.entrySet().stream()
            .filter(Entry::getValue)
            .map(Entry::getKey)
            .collect(Collectors.toList()));
  }
}
