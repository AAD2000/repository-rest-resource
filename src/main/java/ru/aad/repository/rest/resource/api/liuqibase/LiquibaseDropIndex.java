package ru.aad.repository.rest.resource.api.liuqibase;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@JsonInclude(Include.NON_NULL)
public class LiquibaseDropIndex implements Changes {

  private DropIndex dropIndex;

  public LiquibaseDropIndex(ru.aad.repository.rest.resource.api.DropIndex dropIndex) {
    this.dropIndex = new DropIndex(dropIndex.getName(), dropIndex.getTableName());
  }

  public LiquibaseDropIndex(LiquibaseCreateIndex liquibaseCreateIndex) {
    this.dropIndex = new DropIndex(liquibaseCreateIndex.getCreateIndex().getIndexName(),
        liquibaseCreateIndex.getCreateIndex().getTableName());
  }

  @Data
  @AllArgsConstructor
  @JsonInclude(Include.NON_NULL)
  public static class DropIndex {

    private String indexName;

    private String tableName;
  }
}
