package ru.aad.repository.rest.resource.mappers;

import java.util.ArrayList;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

@Mapper(componentModel = "spring", uses = {AddColumnMapper.class, AddSqlMapper.class,
    DropConnectionMapper.class, DropIndexMapper.class, ConnectionMapper.class, CreateIndexMapper.class,
    CreateTableMapper.class, DropColumnMapper.class, DropTableMapper.class})
public interface VersionMapper {


  ru.aad.repository.rest.resource.api.Version model2Dto(
      ru.aad.repository.rest.resource.model.Version model);

  @AfterMapping
  default void afterModel2Dto(@MappingTarget ru.aad.repository.rest.resource.api.Version dto) {
    dto.setAddColumns(dto.getAddColumns() == null ? new ArrayList<>() : dto.getAddColumns());
    dto.setConnections(dto.getConnections() == null ? new ArrayList<>() : dto.getConnections());
    dto.setDropConnections(dto.getDropConnections() == null ? new ArrayList<>() : dto.getDropConnections());
    dto.setDropIndices(dto.getDropIndices() == null ? new ArrayList<>() : dto.getDropIndices());
    dto.setAddSqls(dto.getAddSqls() == null ? new ArrayList<>() : dto.getAddSqls());
    dto.setCreateIndices(dto.getCreateIndices() == null ? new ArrayList<>() : dto.getCreateIndices());
    dto.setCreateTables(dto.getCreateTables() == null ? new ArrayList<>() : dto.getCreateTables());
    dto.setDropColumns(dto.getDropColumns() == null ? new ArrayList<>() : dto.getDropColumns());
    dto.setDropTables(dto.getDropTables() == null ? new ArrayList<>() : dto.getDropTables());
  }

  @Mapping(target = "createdDate", expression = "java(dto.getCreatedDate() == null ? java.time.LocalDateTime.now() : dto.getCreatedDate())")
  ru.aad.repository.rest.resource.model.Version dto2Model(
      ru.aad.repository.rest.resource.api.Version dto);
}
