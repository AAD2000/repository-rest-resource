package ru.aad.repository.rest.resource.model;

import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;

@Data
@Entity
@Table(name = "connection")
public class Connection extends IdInterface {

  @Id
  @Column(name = "id")
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Column(name = "base_table_name")
  private String baseTableName;

  @Column(name = "base_column_names")
  private String baseColumnNames;

  @Column(name = "constraint_name")
  private String constraintName;

  @Column(name = "referenced_table_name")
  private String referencedTableName;

  @Column(name = "referenced_column_names")
  private String referencedColumnNames;

  @Column(name = "created_date")
  private LocalDateTime createdDate;
}
