package ru.aad.repository.rest.resource.api;

import java.time.LocalDateTime;
import lombok.Data;

@Data
public class DropConnection {

  private Long id;

  private String name;

  private String baseTableName;

  private LocalDateTime createdDate;
}
