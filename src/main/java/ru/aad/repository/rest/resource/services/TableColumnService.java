package ru.aad.repository.rest.resource.services;

import java.util.List;
import java.util.stream.Collectors;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.aad.repository.rest.resource.api.TableColumn;
import ru.aad.repository.rest.resource.copiers.Copier;
import ru.aad.repository.rest.resource.mappers.TableColumnMapper;
import ru.aad.repository.rest.resource.model.CreateTable;
import ru.aad.repository.rest.resource.repositories.CreateTableRepository;
import ru.aad.repository.rest.resource.repositories.TableColumnRepository;
import ru.aad.repository.rest.resource.repositories.VersionRepository;

@Service
@AllArgsConstructor
public class TableColumnService {

  private final Copier copier;

  private final TableColumnRepository tableColumnRepository;

  private final VersionRepository versionRepository;

  private final TableColumnMapper tableColumnMapper;

  private final CreateTableRepository createTableRepository;

  public List<TableColumn> getTableColumns(Long ct) {
    return createTableRepository
        .findById(ct)
        .orElseThrow(IllegalArgumentException::new)
        .getTableColumns()
        .stream()
        .map(tableColumnMapper::model2Dto)
        .collect(Collectors.toList());
  }

  public Long create(Long v, Long ct, TableColumn entity) {
    if (versionRepository.findById(v).orElseThrow(IllegalArgumentException::new).isDeployed()) {
      throw new IllegalArgumentException();
    }

    final CreateTable createTable =
        createTableRepository.findById(ct).orElseThrow(IllegalArgumentException::new);

    final ru.aad.repository.rest.resource.model.TableColumn tableColumn =
        tableColumnMapper.dto2Model(entity);

    tableColumn.setCreateTable(createTable);

    return tableColumnRepository.save(tableColumn).getId();
  }

  public void delete(Long id) {
    tableColumnRepository.deleteById(id);
  }

  @Transactional
  public void update(Long v, Long ct, Long id, TableColumn entity) {
    if (versionRepository.findById(v).orElseThrow(IllegalArgumentException::new).isDeployed()) {
      throw new IllegalArgumentException();
    }

    final CreateTable createTable =
        createTableRepository.findById(ct).orElseThrow(IllegalArgumentException::new);

    copier.copy(
        tableColumnRepository.findById(id).orElseThrow(IllegalArgumentException::new),
        tableColumnMapper.dto2Model(entity));
  }
}
