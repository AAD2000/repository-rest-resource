package ru.aad.repository.rest.resource.controllers;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.aad.repository.rest.resource.api.CreateIndexColumn;
import ru.aad.repository.rest.resource.services.IndexColumnService;

@CrossOrigin
@RestController
@AllArgsConstructor
public class IndexColumnController {

  private final IndexColumnService indexColumnService;

  @PostMapping("/index-column")
  public Long create(
      @RequestParam(name = "v") Long v,
      @RequestParam(name = "ct") Long ct,
      @RequestBody CreateIndexColumn entity) {
    return indexColumnService.create(v, ct, entity);
  }

  @DeleteMapping("/index-column/{id}")
  public void delete(@PathVariable(name = "id") Long id) {
    indexColumnService.delete(id);
  }

  @PutMapping("/index-column/{id}")
  public void update(
      @RequestParam(name = "v") Long v,
      @RequestParam(name = "ct") Long ct,
      @PathVariable(name = "id") Long id,
      @RequestBody CreateIndexColumn entity) {
    indexColumnService.update(v, ct, id, entity);
  }
}
