package ru.aad.repository.rest.resource.controllers;

import java.util.List;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.aad.repository.rest.resource.api.DataBaseInfo;
import ru.aad.repository.rest.resource.api.DockerCompose;
import ru.aad.repository.rest.resource.api.FileContentDto;
import ru.aad.repository.rest.resource.services.DockerComposeService;

@CrossOrigin
@RestController
@AllArgsConstructor
@RequestMapping("/docker-compose")
public class DockerComposeController {

  private DockerComposeService dockerComposeService;

  @PostMapping
  public DockerCompose save(@RequestBody DockerCompose dockerCompose) {
    return dockerComposeService.save(dockerCompose);
  }

  @GetMapping
  public List<DockerCompose> getDockerComposes(@RequestParam(name = "owner") String owner) {
    return dockerComposeService.getDockerComposes(owner);
  }

  @GetMapping("/model")
  public DockerCompose getDockerComposes(
      @RequestParam(name = "owner") String owner, @RequestParam(name = "model") String model) {
    return dockerComposeService.getDockerComposes(owner, model);
  }

  @GetMapping("/{id}/files")
  public FileContentDto getFileDatas(
      @PathVariable(name = "id") Long id,
      @RequestParam(name = "owner") String owner,
      @RequestParam(name = "model") String model,
      @RequestParam(name = "rollback", defaultValue = "false") boolean rollback,
      @RequestParam(name = "tag", required = false) String tag,
      @RequestParam(name = "force", defaultValue = "false") boolean force) {
    return dockerComposeService.getFileData(id, owner, model, rollback, tag, force);
  }

  @DeleteMapping("/{id}")
  public boolean delete(@PathVariable(name = "id") Long id) {
    return dockerComposeService.delete(id);
  }

  @GetMapping("/url/{id}")
  public DataBaseInfo dataBaseInfo(@PathVariable(name = "id") Long id) {
    return dockerComposeService.dataBaseInfo(id);
  }

  @PutMapping("/{id}/after-deploy")
  public boolean afterDeploy(
      @PathVariable(name = "id") Long id,
      @RequestParam(name = "owner") String owner,
      @RequestParam(name = "model") String model,
      @RequestParam(name = "rollback", defaultValue = "false") boolean rollback,
      @RequestParam(name = "tag", required = false) String tag) {
    return dockerComposeService.afterDeploy(id, owner, model, rollback, tag);
  }

  @GetMapping("/{id}")
  public DockerCompose dockerCompose(@PathVariable(name = "id") Long id) {
    return dockerComposeService.getDockerCompose(id);
  }

  @GetMapping("/ports")
  public List<Long> getUsedPorts() {
    return dockerComposeService.usedPorts();
  }
}
