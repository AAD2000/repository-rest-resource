package ru.aad.repository.rest.resource.api;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;

@Data
@JsonSerialize
public class EmptyObject {

}
