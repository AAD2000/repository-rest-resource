package ru.aad.repository.rest.resource.mappers;

import java.util.ArrayList;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

@Mapper(componentModel = "spring", uses = TableColumnMapper.class)
public interface CreateTableMapper {

  ru.aad.repository.rest.resource.api.CreateTable model2Dto(
      ru.aad.repository.rest.resource.model.CreateTable model);

  @AfterMapping
  default void afterModel2Dto(@MappingTarget ru.aad.repository.rest.resource.api.CreateTable dto) {
    dto.setTableColumns(dto.getTableColumns() == null ? new ArrayList<>() : dto.getTableColumns());
  }

  @Mapping(target = "createdDate", expression = "java(dto.getCreatedDate() == null ? java.time.LocalDateTime.now() : dto.getCreatedDate())")
  ru.aad.repository.rest.resource.model.CreateTable dto2Model(
      ru.aad.repository.rest.resource.api.CreateTable dto);
}
