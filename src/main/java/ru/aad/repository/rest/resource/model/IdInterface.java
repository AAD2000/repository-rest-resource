package ru.aad.repository.rest.resource.model;

public abstract class IdInterface {

  public abstract Long getId();
}
