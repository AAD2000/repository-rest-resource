FROM adoptopenjdk/openjdk11

COPY /build/libs/repository-rest-resource-0.0.1-SNAPSHOT.jar repository-rest-resource-0.0.1-SNAPSHOT.jar

CMD ["java", "-jar", "repository-rest-resource-0.0.1-SNAPSHOT.jar"]